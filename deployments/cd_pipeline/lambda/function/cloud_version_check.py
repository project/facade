import os

import boto3
import requests
import semver

TAGS_API_URL = os.environ['CLOUD_ORCHESTRATOR_URL']
ACCOUNT_ID = os.environ['ACCOUNT_ID']
REPOSITORY_NAME = os.environ['REPOSITORY_NAME']

def main():
  cloud_latest_tag = get_cloud_tags(TAGS_API_URL)
  ecr_latest_tag = get_ecr_tag(ACCOUNT_ID,REPOSITORY_NAME)
  if((semver.compare(cloud_latest_tag,ecr_latest_tag)) == 1 ):
    print('You have a new version.')
    return cloud_latest_tag
  else:
    print('You do not have a new version.')
    return None

def get_cloud_tags(tagapiurl=TAGS_API_URL):
  response = requests.get(tagapiurl)
  response.raise_for_status()
  tags = response.json()
  latest_tag = max(tags,key=lambda x: x['name'])['name']
  return latest_tag

def get_ecr_tag(registryid=ACCOUNT_ID,repositoryname=REPOSITORY_NAME):
  # get image list from ECR
  ecr = boto3.client('ecr')
  imageinfo = ecr.list_images(registryId=registryid,repositoryName=repositoryname)
  images = imageinfo.get('imageIds')
  image_tags = [d['imageTag'] for d in images if 'imageTag' in d]
  semver_tags = create_semver_list(image_tags)

  if len(semver_tags) == 0:
    return '0.0.1'

  # return latest version
  return max(semver_tags)

def create_semver_list(image_tags):
  semver_list = []
  for tag in image_tags:
    if(is_semver(tag) != None):
      semver_list.append(tag)
  return semver_list

def is_semver(tag):
  try:
    ver = semver.Version.parse(tag)
  except Exception:
    return None
  else:
    return tag
