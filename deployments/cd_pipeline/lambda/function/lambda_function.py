import os
import boto3
import cloud_version_check
import zipfile
import io
import logging

s3 = boto3.resource('s3')
S3_BUCKET_NAME = os.environ['S3_BUCKET_NAME']
SOURCE_DIR = os.environ['SOURCE_DIR']
BASE_ENV_FILE = os.environ['BASE_ENV_FILE']
BASE_BUILDSPEC_FILE = os.environ['BASE_BUILDSPEC_FILE']
OUTPUT_ENV_FILE = os.environ['OUTPUT_ENV_FILE']
OUTPUT_BUILDSPEC_FILE = os.environ['OUTPUT_BUILDSPEC_FILE']
ZIP_FILE_NAME = 'source.zip'

def lambda_handler(event, context):
  replace_txt = cloud_version_check.main()

  if replace_txt is None:
    print("You do not have a new version.")
    return

  replace_file(BASE_ENV_FILE,OUTPUT_ENV_FILE,replace_txt)
  replace_file(BASE_BUILDSPEC_FILE,OUTPUT_BUILDSPEC_FILE,replace_txt)

  print('create zip file')
  bucket = s3.Bucket(S3_BUCKET_NAME)
  files = []
  for obj in bucket.objects.filter(Prefix=SOURCE_DIR):
    key = obj.key
    if key == SOURCE_DIR + '/' + 'base' + '/' or key.startswith(SOURCE_DIR + '/' + 'base' + '/'):
      continue
    if key == SOURCE_DIR:
      continue
    if key != SOURCE_DIR + '/':
      files.append(key)

  # compress zipfile
  try:
    with io.BytesIO() as zip_buffer:
      with zipfile.ZipFile(zip_buffer, 'w', zipfile.ZIP_DEFLATED) as zip_file:
        for file_key in files:
          try:
            obj = s3.Object(S3_BUCKET_NAME, file_key)
            file_data = obj.get()['Body'].read()
            zip_file.writestr(file_key[len(SOURCE_DIR) + 1:], file_data)
          except Exception as e:
            logging.error(f"Error processing file {file_key}: {e}")
            continue

      # upload zipfile
      zip_buffer.seek(0)
      try:
        bucket.upload_fileobj(zip_buffer, ZIP_FILE_NAME)
      except Exception as e:
        logging.error(f"Error uploading zip file: {e}")
        return {
          'statusCode': 500,
          'body': f'Error occurred: {e}'
        }

    return {
      'statusCode': 200,
      'body': 'Created source file'
    }

  except Exception as e:
    logging.error(f"Error compressing files: {e}")
    return {
      'statusCode': 500,
      'body': f'Error occurred: {e}'
    }

def replace_file(inputfile,outputfile,replace_txt):
  try:
    print("start s3 file read")
    bucket = s3.Bucket(S3_BUCKET_NAME)
    obj = bucket.Object(inputfile)
    response = obj.get()
    body = response['Body'].read().decode('utf-8')
  except Exception as e:
    print('I could not get file')
    return

  print("end s3 file read")
  new_body = body.replace('{tag}',replace_txt)
  print("start upload file")

  try:
    output_bucket = s3.Bucket(S3_BUCKET_NAME)
    output_obj = output_bucket.Object(outputfile)
    output_obj.put(Body=new_body)
  except Exception as e:
    print('I could not upload file')

  print('end upload file')
  return
