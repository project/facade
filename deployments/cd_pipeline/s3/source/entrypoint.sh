#!/bin/bash

source "$(pwd)/.env"

readonly ROOT_PATH='/var/www'
readonly DRUPAL_ROOT="${ROOT_PATH}/cloud_orchestrator/app"
readonly DOCROOT="${DRUPAL_ROOT}/docroot"

readonly FACADE_FILES_DIR="${DOCROOT}/sites/${FACADE_HOST}/files"
readonly FACADE_SETTINGS_FILE="${DOCROOT}/sites/${FACADE_HOST}/settings.php"
readonly FACADE_SERVICES_FILE="${DOCROOT}/sites/${FACADE_HOST}/services.yml"
readonly FACADE_PRIVATE_DIR="${DRUPAL_ROOT}/files/${FACADE_HOST}"
readonly KEYS_DIR="${PRIVATE_DIR}/keys"
readonly EMAIL_TXT_FILE="/tmp/email.txt"

readonly CO_FILES_DIR="${DOCROOT}/sites/${CO_HOST}/files"
readonly CO_SETTINGS_FILE="${DOCROOT}/sites/${CO_HOST}/settings.php"
readonly CO_SERVICES_FILE="${DOCROOT}/sites/${CO_HOST}/services.yml"
readonly CO_PRIVATE_DIR="${DRUPAL_ROOT}/files/${CO_HOST}"

echo "PATH=$PATH:${DRUPAL_ROOT}/vendor/bin" >> /root/.bashrc
source /root/.bashrc

/usr/sbin/rsyslogd -n &
service postfix start

# Install Façade.

# set COMPOSER_ALLOW_SUPERUSER=1 since Docker is run as root.
export COMPOSER_ALLOW_SUPERUSER=1

# Only download for Cloud Orchestrator 8.x and higher.  Lower version already
# have drupal/facade as part of composer.json.
if [[ "$CO_VERSION" == 7* ]]; then
  if ! composer show -s | grep -q "drupal/facade"; then
    # In order to install drupal/facade into the correct location,
    composer config --global github-oauth.github.com "${GITHUB_TOKEN}"
    composer require drupal/facade:^2
  fi
fi

# Create settings.php.
if [ ! -e "${FACADE_SETTINGS_FILE}" ]; then

  echo 'Setting up Façade module.'

  mkdir -p "${FACADE_FILES_DIR}"
  mkdir -p "${FACADE_PRIVATE_DIR}"

  cp "${DOCROOT}/sites/default/default.settings.php" "${FACADE_SETTINGS_FILE}"
  chmod 666 "${FACADE_SETTINGS_FILE}"

  # Create services.yml.
  cp "${DOCROOT}/sites/default/default.services.yml" \
     "${FACADE_SERVICES_FILE}";

  sed -i "s/gc_divisor: .*$/gc_divisor: 1/" "${FACADE_SERVICES_FILE}"
  sed -i "s/gc_maxlifetime: .*$/gc_maxlifetime: 3600/" "${FACADE_SERVICES_FILE}"
  sed -i "s/cookie_lifetime: .*$/cookie_lifetime: 0/" "${FACADE_SERVICES_FILE}"

  echo "\$settings['file_private_path'] = '${FACADE_PRIVATE_DIR}';" >> "${FACADE_SETTINGS_FILE}"

  # Make the drush directory and setup the default uri.
  mkdir ${DRUPAL_ROOT}/drush
  tee -a ${DRUPAL_ROOT}/drush/drush.yml >/dev/null << EOF
options:
  uri:  https://${FACADE_HOST}
EOF
  chown -R www-data:www-data ${DRUPAL_ROOT}/drush

  # Add private keys directory
  mkdir -p ${KEYS_DIR}
  SITE_EMAIL="no-reply@${FACADE_HOST}"

  drush si -y \
    --db-url="mysql://${MYSQL_USERNAME}:${MYSQL_PASSWORD}@${MYSQL_ADDRESS}:${MYSQL_PORT}/${FACADE_DATABASE_NAME}" \
    --site-name="${FACADE_SITE_NAME:-Facade}" \
    --site-mail="${SITE_EMAIL}" \
    --sites-subdir="${FACADE_HOST}" \
    cloud_orchestrator \
    cloud_orchestrator_module_configure_form.install_spa=NULL

  # Change the output destination of Drupal logs from database to syslog.
  drush --uri="http://${FACADE_HOST}" en -y syslog
  drush --uri="http://${FACADE_HOST}" un -y dblog
  drush --uri="http://${FACADE_HOST}" config:set system.logging error_level hide -y

  # Block uid=1 for best practice.
  drush --uri="http://${FACADE_HOST}" ublk admin

  # Create the Façade user.
  drush --uri="http://${FACADE_HOST}" ucrt ${FACADE_ADMIN_ID} --mail ${FACADE_ADMIN_EMAIL}
  drush --uri="http://${FACADE_HOST}" urol 'administrator' ${FACADE_ADMIN_ID}

  # Switch to Claro admin theme.
  cd "${DRUPAL_ROOT}/vendor/bin"

  # Set local timezone.
  drush --uri="http://${FACADE_HOST}" -y config:set system.date timezone.default "${FACADE_TIMEZONE:-Asia/Tokyo}"

  # Switch to bootstrap_cloud
  drush --uri="http://${FACADE_HOST}" then -y bootstrap_cloud
  drush --uri="http://${FACADE_HOST}" cset -y system.theme default bootstrap_cloud

  # switch to /tenant as the homepage.
  drush --uri="http://${FACADE_HOST}" cset -y system.site page.front '/facade/tenant'
  drush --uri="http://${FACADE_HOST}" en -y memcache memcache_admin
  drush --uri="http://${FACADE_HOST}" -y cr

  drush --uri="http://${FACADE_HOST}" en -y openstack_provider

  # Setup additional trusted host patterns for ELB health check.
  # ELB health checks access the IP directly and not the DNS name.

  chmod 666 ${FACADE_SETTINGS_FILE}
  tee -a "${FACADE_SETTINGS_FILE:-${DOCROOT}/sites/default/settings.php}" >/dev/null << EOF
\$settings['memcache']['servers'] = ['${MEMCACHE_ADDRESS:-127.0.0.1}:${MEMCACHE_PORT:-11211}' => 'default'];
\$settings['memcache']['bins'] = ['default' => 'default'];
\$settings['memcache']['key_prefix'] = 'facade';
\$settings['memcache']['options'] = [
  Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT,
];
\$settings['cache']['default'] = 'cache.backend.memcache';

\$settings['trusted_host_patterns'] = [
  '^${FACADE_HOST//./\\.}$',
  '^10\.\d{1,3}\.\d{1,3}\.\d{1,3}$',
  '^172\.(1[6-9]|2[0-9]|3[0-1])\.\d{1,3}\.\d{1,3}$',
  '^192\.168\.\d{1,3}\.\d{1,3}$',
];

# ALB's IP address changes, so all IP addresses are allowed.
# There is no security problem because the ECS security group only accepts communications from ALB.
\$settings['reverse_proxy'] = TRUE;
\$settings['reverse_proxy_addresses'] = ['0.0.0.0/0'];
\$settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_FOR | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_HOST | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PORT | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PROTO | \Symfony\Component\HttpFoundation\Request::HEADER_FORWARDED;
EOF
  chmod 444 ${FACADE_SETTINGS_FILE}

  drush --uri="http://${FACADE_HOST}" -y cr

  LOGIN_LINK=$(drush --uri="http://${FACADE_HOST}" uli --name ${FACADE_ADMIN_ID})
  # Send out welcome email message.
  EXPANDED_EMAIL_MESSAGE=$(awk -v facade_admin_id="${FACADE_ADMIN_ID}" -v login_link="${LOGIN_LINK}" -v dns_name="${DNS_NAME}" '{ gsub(/\${FACADE_ADMIN_ID}/, facade_admin_id); gsub(/\${LOGIN_LINK}/, login_link); gsub(/\${DNS_NAME}/, dns_name); printf "%s", $0 }' <<< ${WELCOME_MESSAGE})

  echo -e ${EXPANDED_EMAIL_MESSAGE} > "${EMAIL_TXT_FILE}"
  mail -s "${WELCOME_SUBJECT}" --append="FROM: ${FACADE_SITE_NAME} <${SITE_EMAIL}>" ${FACADE_ADMIN_EMAIL} < "${EMAIL_TXT_FILE}"

  rm -f /etc/crontab

  # Setup crontab.
  # Enable drush cron.
  DRUSH_QUEUE_RUN_SCRIPT="${DOCROOT}/modules/contrib/cloud/scripts/drush_queue_run.sh"
  chmod +x "${DRUSH_QUEUE_RUN_SCRIPT}"
  tee /etc/crontab > /dev/null << EOF
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:${DRUPAL_ROOT}/vendor/bin
MAILTO=''
*/1 * * * * www-data cd '${DOCROOT}'; drush --uri="http://${FACADE_HOST}" cron > /dev/null 2>&1
*/15 * * * * www-data cd '${DOCROOT}'; '${DRUSH_QUEUE_RUN_SCRIPT}' > /dev/null 2>&1
EOF
  # Workaround for error when running cron as www-data
  # https://stackoverflow.com/questions/43323754/cannot-make-remove-an-entry-for-the-specified-session-cron
  sed -i '/session    required     pam_loginuid.so/c\#session    required   pam_loginuid.so' /etc/pam.d/cron

  echo 'Façade installation complete.'
fi

# Install Cloud Orchestrator.
# Create settings.php.
if [ ! -e "${CO_SETTINGS_FILE}" ]; then

  echo 'Copy settings.php and install.'

  mkdir -p "${CO_FILES_DIR}"
  mkdir -p "${CO_PRIVATE_DIR}"

  cp "${DRUPAL_ROOT}/docroot/sites/default/default.settings.php" "${CO_SETTINGS_FILE}"
  chmod 666 "${CO_SETTINGS_FILE}"
  cp "${DRUPAL_ROOT}/docroot/sites/example.sites.php" "${DRUPAL_ROOT}/docroot/sites/sites.php"

  echo "\$settings['file_private_path'] = '${CO_PRIVATE_DIR}';" >> "${CO_SETTINGS_FILE}"

  # Add new trusted_host_patterns configuration and Escape the . included in the domain
  # Enable reverse proxy settings.
  tee -a "${CO_SETTINGS_FILE:-${DOCROOT}/sites/default/settings.php}" > /dev/null << EOF
\$settings['trusted_host_patterns'] = [
  '^${CO_HOST//./\\.}$',
  '^10\.\d{1,3}\.\d{1,3}\.\d{1,3}$',
  '^172\.(1[6-9]|2[0-9]|3[0-1])\.\d{1,3}\.\d{1,3}$',
  '^192\.168\.\d{1,3}\.\d{1,3}$',
];

# ALB's IP address changes, so all IP addresses are allowed.
# There is no security problem because the ECS security group only accepts communications from ALB.
\$settings['reverse_proxy'] = TRUE;
\$settings['reverse_proxy_addresses'] = ['0.0.0.0/0'];
\$settings['reverse_proxy_trusted_headers'] = \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_FOR | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_HOST | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PORT | \Symfony\Component\HttpFoundation\Request::HEADER_X_FORWARDED_PROTO | \Symfony\Component\HttpFoundation\Request::HEADER_FORWARDED;
EOF

  drush si -y \
    "--db-url=mysql://${MYSQL_USERNAME:-cloud_admin}:${MYSQL_PASSWORD:-cloud_admin}@${MYSQL_ADDRESS:-127.0.0.1}:${MYSQL_PORT:-3306}/${CO_DATABASE_NAME:-cloud_orchestrator}" \
    "--account-name=${CO_ADMIN_ID:-cloud_admin}" \
    "--account-pass=${CO_ADMIN_PASSWORD:-cloud_admin}" \
    "--account-mail=${CO_ADMIN_EMAIL:-no-reply@example.com}" \
    "--sites-subdir=${CO_HOST}" \
    cloud_orchestrator \
    cloud_orchestrator_module_configure_form.cloud_service_providers.terraform=terraform \
    cloud_orchestrator_module_configure_form.cloud_service_providers.openstack=openstack \
    cloud_orchestrator_module_configure_form.cloud_service_providers.vmware=vmware

  cd "${DOCROOT}"
  chown -R www-data:www-data "${CO_FILES_DIR}"
  chmod 777 "${CO_FILES_DIR}"

  # Switch to Claro admin theme.
  cd "${DRUPAL_ROOT}/vendor/bin"

  # Set local timezone.
  drush --uri="http://${CO_HOST}" -y config:set system.date timezone.default "${CO_TIMEZONE:-Asia/Tokyo}"

  # Switch to Claro Admin.
  drush --uri="http://${CO_HOST}" then -y claro
  drush --uri="http://${CO_HOST}" cset -y system.theme admin claro

  drush --uri="http://${CO_HOST}" en -y memcache memcache_admin
  drush --uri="http://${CO_HOST}" -y cr

  tee -a "${CO_SETTINGS_FILE:-${DOCROOT}/sites/default/settings.php}" > /dev/null << EOF
\$settings['memcache']['servers'] = ['${MEMCACHE_ADDRESS:-127.0.0.1}:${MEMCACHE_PORT:-11211}' => 'default'];
\$settings['memcache']['bins'] = ['default' => 'default'];
\$settings['memcache']['key_prefix'] = 'cloud_orchestrator';
\$settings['memcache']['options'] = [
  Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT,
];
\$settings['cache']['default'] = 'cache.backend.memcache';
EOF

  drush --uri="http://${CO_HOST}" -y cr

  # Change the output destination of Drupal logs from database to syslog.
  drush --uri="http://${CO_HOST}" en -y syslog
  drush --uri="http://${CO_HOST}" un -y dblog
  drush --uri="http://${CO_HOST}" config:set system.logging error_level hide -y

  # Enable the TFA module and Real AES module using Drush.
  drush --uri="http://${CO_HOST}" en -y tfa
  drush --uri="http://${CO_HOST}" en -y real_aes

  # Define the directory path for private files.
  PRIVATE_DIRECTORY="${DOCROOT}/sites/${CO_HOST}/files/private/tfa"
  # Create the private directory if it doesn't exist and set ownership.
  mkdir -p "${PRIVATE_DIRECTORY}"
  chown www-data. "${PRIVATE_DIRECTORY}"
  # Generate a random encryption key using /dev/urandom, base64 encode it, and save it to a file.
  ENCRYPTION_KEY_FILE="${PRIVATE_DIRECTORY}/tfa_encrypt.key"
  dd if=/dev/urandom bs=32 count=1 | base64 -i - > "${ENCRYPTION_KEY_FILE}"
  chown www-data:www-data "${ENCRYPTION_KEY_FILE}"

  CONFIG_FILE_DIRECTORY="${CO_FILES_DIR}/config/install"
  # Replace with the path of the encryption key created.
  sed -i 's#sites/example.com/files/private/tfa_encrypt.key#'"${ENCRYPTION_KEY_FILE}"'#' "${CONFIG_FILE_DIRECTORY}/key.key.tfa_key.yml"

  # Import config using 'drush' with options for partial imports, source dir.
  drush --uri="http://${CO_HOST}" config-import --partial --source="${CONFIG_FILE_DIRECTORY}" -y

  echo "Finish installing Cloud Orchestrator."

fi

echo "Start Cloud Orchestrator cron setting."

rm -f /etc/crontab

# DISABLE_DRUSH_CRON is set from the container's environment variable.
# If you want to skip the drush cron processing, then set a environment
# variable DISABLE_DRUSH_CRON of the container (the value can be any value
# such as 'TRUE').
if [ "x${DISABLE_DRUSH_CRON}" != 'x' ]; then

  # Disable drush cron.
  echo "DISABLE_DRUSH_CRON=${DISABLE_DRUSH_CRON}"

else

  # Setup crontab.
  # Enable drush cron.
  DRUSH_QUEUE_RUN_SCRIPT="${DOCROOT}/modules/contrib/cloud/scripts/drush_queue_run.sh"
  chmod +x "${DRUSH_QUEUE_RUN_SCRIPT}"
  tee /etc/crontab > /dev/null << EOF
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
MAILTO=''
*/1 * * * * www-data cd '${DRUPAL_ROOT}/docroot'; drush --uri="http://${CO_HOST}" cron > /dev/null 2>&1
*/15 * * * * www-data cd '${DRUPAL_ROOT}/docroot; '${DRUSH_QUEUE_RUN_SCRIPT}' > /dev/null 2>&1
EOF

fi
echo "Finish Cloud Orchestrator cron setting."

ln -s "${DRUPAL_ROOT}/docroot" /var/www/cloud_orchestrator

cd "${DRUPAL_ROOT}/docroot"
# Change docroot permissions.
chown -R root:www-data .
find . -type d -exec chmod u=rwx,g=rx,o= '{}' \;
find . -type f -exec chmod u=rw,g=r,o= '{}' \;

# Change permissions on docroot/sites/*/files directory.
cd "${DOCROOT}/sites"
find . -type d -name files -exec chmod ug=rwx,o= '{}' \;
find ./*/files -type d -exec chmod ug=rwx,o= '{}' \;
find ./*/files -type f -exec chmod ug=rw,o= '{}' \;

# Change permissions on private directory.
cd "${DRUPAL_ROOT}/files"
chown -R www-data:www-data .
chmod 700 .

# Move files that give information to an attacker out of the docroot.
mv "${DRUPAL_ROOT}/docroot/example.gitignore" "${DRUPAL_ROOT}"
mv "${DRUPAL_ROOT}/docroot/INSTALL.txt" "${DRUPAL_ROOT}"
mv "${DRUPAL_ROOT}/docroot/README.md" "${DRUPAL_ROOT}"
mv "${DRUPAL_ROOT}/docroot/update.php" "${DRUPAL_ROOT}"
mv ${DRUPAL_ROOT}/docroot/core/*.txt "${DRUPAL_ROOT}"
mv "${DRUPAL_ROOT}/docroot/core/install.php" "${DRUPAL_ROOT}"

# Start cron.
service cron start

apache2-foreground
