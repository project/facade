#!/bin/bash

source "$(pwd)/.env"

echo "IMAGE_TAG=$IMAGE_TAG" > .image_tag 

docker build \
  --build-arg DISABLE_DRUSH_CRON="${DISABLE_DRUSH_CRON:-}" \
  --build-arg GITHUB_TOKEN="${GITHUB_TOKEN}" \
  --build-arg CO_VERSION="${CO_VERSION}" \
  --build-arg CO_HOST="${CO_HOST}" \
  --build-arg FACADE_HOST="${FACADE_HOST}" \
  -t "${IMAGE_TAG}:latest" \
  .
