# User Guide

This directory contains CD pipeline resources.

## AWS Lambda

AWS Lambda workflow:

1. AWS Lambda compares the [cloud](https://drupal.org/project/cloud) module with
   the version of the tag in ECR.
2. If the [cloud](https://drupal.org/project/cloud) module is newer, refer to
   `buildspec.yml` from S3 at `<bucket-name>/poc/base/buildspec.yml` and create
   a new `buildspec.yml` in S3 at `<bucket-name>/poc/buildspec.yml`.
3. Compress the new `buildspec.yml` and `Dockerfile` into a zip file and upload
   it to S3 at `<bucket-name>/poc/source.zip`.

## Amazon S3

The following resources will be created:

- `<bucket-name>/poc/Dockerfile`
- `<bucket-name>/poc/base/buildspec.yml`
  - The source of the new `buildspec.yml` output
