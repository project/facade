#!/bin/bash

readonly ROOT_PATH='/var/www'
readonly FACADE_DIR="${ROOT_PATH}/facade"
readonly DOCROOT="${FACADE_DIR}/docroot"
readonly PRIVATE_DIR="${DOCROOT}/sites/default/files/private"
readonly KEYS_DIR="${PRIVATE_DIR}/keys"
readonly FILES_DIR="${DOCROOT}/sites/default/files"
readonly SETTINGS_FILE="${DOCROOT}/sites/default/settings.php"
readonly SERVICES_FILE="${DOCROOT}/sites/default/services.yml"
readonly EMAIL_TXT_FILE="/tmp/email.txt"

echo "PATH=$PATH:${FACADE_DIR}/vendor/bin" >> /root/.bashrc
source /root/.bashrc

service rsyslog start
service postfix start

# Create settings.php.
if [ ! -e "${SETTINGS_FILE}" ]; then

  echo 'Setting up Façade module.'

  cp "${DOCROOT}/sites/default/default.settings.php" \
     "${SETTINGS_FILE}";
  chmod 666 "${SETTINGS_FILE}"

  # Create services.yml.
  cp "${DOCROOT}/sites/default/default.services.yml" \
     "${SERVICES_FILE}";

  sed -i "s/gc_divisor: .*$/gc_divisor: 1/" "${SERVICES_FILE}"
  sed -i "s/gc_maxlifetime: .*$/gc_maxlifetime: 3600/" "${SERVICES_FILE}"
  sed -i "s/cookie_lifetime: .*$/cookie_lifetime: 0/" "${SERVICES_FILE}"

  echo "\$settings['file_private_path'] = '${PRIVATE_DIR}';" >> "${SETTINGS_FILE}"

  # Make the drush directory and setup the default uri.
  mkdir ${FACADE_DIR}/drush
  tee -a ${FACADE_DIR}/drush/drush.yml >/dev/null << EOF
options:
  uri:  https://${DNS_NAME}
EOF
  chown -R www-data:www-data ${FACADE_DIR}/drush

  # Add private keys directory
  mkdir -p ${KEYS_DIR}
  SITE_EMAIL="no-reply@${DNS_NAME}"

  # Pull the password out of the secret passed.
  MYSQL_PASSWORD=$(echo ${MYSQL_SECRET} | jq -r '.password')

  drush si -y \
    --db-url="mysql://${MYSQL_USERNAME}:${MYSQL_PASSWORD}@${MYSQL_ADDRESS}:${MYSQL_PORT}/${MYSQL_DATABASE_NAME}" \
    --site-name="${FACADE_SITE_NAME:-Facade}" \
    --site-mail="${SITE_EMAIL}" \
    cloud_orchestrator \
    cloud_orchestrator_module_configure_form.install_spa=NULL

  # Change the output destination of Drupal logs from database to syslog.
  drush en -y syslog
  drush un -y dblog
  drush config:set system.logging error_level all -y

  # Block uid=1 for best practice.
  drush ublk admin

  # Create the Facade user.
  drush ucrt ${FACADE_ADMIN_ID} --mail ${FACADE_ADMIN_EMAIL}
  drush urol 'administrator' ${FACADE_ADMIN_ID}

  # Switch to Claro admin theme.
  cd "${FACADE_DIR}/vendor/bin"

  # Set local timezone.
  drush -y config:set system.date timezone.default "${FACADE_TIMEZONE:-'Asia/Tokyo'}"

  # Switch to bootstrap_cloud
  drush then -y bootstrap_cloud
  drush cset -y system.theme default bootstrap_cloud

  # switch to /tenant as the homepage.
  drush cset -y system.site page.front '/facade/tenant'
  drush en -y memcache memcache_admin
  drush -y cr

  drush en -y openstack_provider

  # Setup additional trusted host patterns for ELB health check.
  # ELB health checks access the IP directly and not the DNS name.
  TRUSTED_IPS=''
  for IP in $(getent hosts | cut -d' ' -f1)
  do
    TRUSTED_IPS+="'^${IP//./\\.}$',"$'\n'
  done

  chmod 666 ${SETTINGS_FILE}
  tee -a "${SETTINGS_FILE:-${DOCROOT}/sites/default/settings.php}" >/dev/null << EOF
\$settings['memcache']['servers'] = ['${MEMCACHE_ADDRESS:-127.0.0.1}:${MEMCACHE_PORT:-11211}' => 'default'];
\$settings['memcache']['bins'] = ['default' => 'default'];
\$settings['memcache']['key_prefix'] = 'facade';
\$settings['memcache']['options'] = [
  Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_CONSISTENT,
];
\$settings['cache']['default'] = 'cache.backend.memcache';

\$settings['trusted_host_patterns'] = [
  '^${DNS_NAME//./\\.}$',
  ${TRUSTED_IPS}
];
EOF
  chmod 444 ${SETTINGS_FILE}

  drush -y cr

  LOGIN_LINK=$(drush uli --name ${FACADE_ADMIN_ID})
  # Send out welcome email message.
  EXPANDED_EMAIL_MESSAGE=$(awk -v facade_admin_id="${FACADE_ADMIN_ID}" -v login_link="${LOGIN_LINK}" -v dns_name="${DNS_NAME}" '{ gsub(/\${FACADE_ADMIN_ID}/, facade_admin_id); gsub(/\${LOGIN_LINK}/, login_link); gsub(/\${DNS_NAME}/, dns_name); printf "%s", $0 }' <<< ${WELCOME_MESSAGE})

  echo -e ${EXPANDED_EMAIL_MESSAGE} > "${EMAIL_TXT_FILE}"
  mail -s "${WELCOME_SUBJECT}" --append="FROM: ${FACADE_SITE_NAME} <${SITE_EMAIL}>" ${FACADE_ADMIN_EMAIL} < "${EMAIL_TXT_FILE}"

  rm -f /etc/crontab

  # Setup crontab.
  # Enable drush cron.
  DRUSH_QUEUE_RUN_SCRIPT="${DOCROOT}/modules/contrib/cloud/scripts/drush_queue_run.sh"
  chmod +x "${DRUSH_QUEUE_RUN_SCRIPT}"
  tee /etc/crontab > /dev/null << EOF
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:${FACADE_DIR}/vendor/bin
MAILTO=''
*/1 * * * * www-data cd '${DOCROOT}'; drush cron > /dev/null 2>&1
*/15 * * * * www-data cd '${DOCROOT}'; '${DRUSH_QUEUE_RUN_SCRIPT}' > /dev/null 2>&1
EOF
  # Workaround for error when running cron as www-data
  # https://stackoverflow.com/questions/43323754/cannot-make-remove-an-entry-for-the-specified-session-cron
  sed -i '/session    required     pam_loginuid.so/c\#session    required   pam_loginuid.so' /etc/pam.d/cron

  # Start cron.
  service cron start

  echo 'Façade installation complete.'
fi

apache2-foreground

