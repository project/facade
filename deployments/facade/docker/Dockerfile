# Create common part as base.
# Façade Dockerfile.
FROM php:8.3-apache-bookworm as base

ENV ROOT_PATH='/var/www'
ENV FACADE_DIR="${ROOT_PATH}/facade"
ENV DOCROOT="${FACADE_DIR}/docroot"

RUN set -eux; \
  if command -v a2enmod; then \
    a2enmod rewrite; \
  fi; \
  savedAptMark="$(apt-mark showmanual)"; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
    git \
    cron \
    curl \
    jq \
    libfreetype6-dev \
    libjpeg-dev \
    libmagickwand-dev \
    libmemcached-dev \
    libpng-dev \
    libpq-dev \
    libssl-dev \
    libwebp-dev \
    libzip-dev \
    unzip \
    zlib1g \
    zip

# Install APCU libraries.
RUN git clone https://github.com/krakjoe/apcu /usr/src/php/ext/apcu \
    && cd /usr/src/php/ext/apcu \
    && docker-php-ext-install apcu > /dev/null 2>&1

# Install OpCache.
RUN docker-php-ext-install -j "$(nproc)" opcache > /dev/null 2>&1

# Install uploadprogress.
RUN pecl install uploadprogress > /dev/null 2>&1 \
    && docker-php-ext-enable uploadprogress > /dev/null 2>&1

# Install Memcached.
RUN git clone https://github.com/php-memcached-dev/php-memcached /usr/src/php/ext/memcached \
    && cd /usr/src/php/ext/memcached \
    && docker-php-ext-install memcached > /dev/null 2>&1

# Install graphic libraries.
RUN docker-php-ext-configure gd \
      --with-freetype \
      --with-jpeg \
      --with-webp > /dev/null 2>&1

RUN docker-php-ext-install -j "$(nproc)" \
      gd \
      opcache \
      pdo_mysql \
      zip \
      mysqli > /dev/null 2>&1

RUN mv /etc/localtime /etc/localtime.org \
  && ln -s /usr/share/zoneinfo/Asia/Tokyo /etc/localtime;

# Prep for Postfix.
RUN apt-get install -y --no-install-recommends \
      mailutils \
      rsyslog > /dev/null 2>&1

# Change rsyslog settings to output Drupal logs.
RUN { \
  echo 'local0.* -/proc/1/fd/1'; \
} >> /etc/rsyslog.conf
RUN sed -i 's/^module(load="imklog")/#&/' /etc/rsyslog.conf \

# Setup Postfix and rsyslog.
RUN set -eux; \
  HOSTNAME="$(hostname)"; \
  echo "postfix postfix/mailname string ${HOSTNAME}" | debconf-set-selections; \
  echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections; \
  DEBIAN_FRONTEND=noninteractive apt-get install --assume-yes postfix > /dev/null 2>&1; \
  service rsyslog start; \
  service postfix restart > /dev/null 2>&1

# Façade PHP configurations.
RUN { \
  echo 'memory_limit = -1'; \
  echo 'max_execution_time = 600'; \
  echo 'max_input_time = 600'; \
  echo 'max_input_vars = 100000'; \
  echo 'output_buffering = 4096'; \
  echo 'error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT'; \
} > /usr/local/etc/php/conf.d/extras.ini

RUN a2enmod headers
RUN { \
  echo "<VirtualHost *:80>"; \
  echo "  DocumentRoot ${DOCROOT}"; \
  echo "  <Directory />"; \
  echo "    Options FollowSymLinks"; \
  echo "    AllowOverride None"; \
  echo "  </Directory>"; \
  echo "  <Directory ${DOCROOT}>"; \
  echo "    <LimitExcept GET POST>"; \
  echo "      Require all denied"; \
  echo "    </LimitExcept>"; \
  echo "    Options FollowSymLinks MultiViews"; \
  echo "    AllowOverride All"; \
  echo "    order allow,deny"; \
  echo "    allow from all"; \
  echo "  </Directory>"; \
  echo "  <IfModule mod_headers.c>"; \
  echo "    Header set X-Frame-Options \"DENY\""; \
  echo "  </IfModule>"; \
  echo "  ErrorLog /dev/stdout"; \
  echo "  LogLevel warn"; \
  echo "  CustomLog /dev/stdout combined"; \
  echo "</VirtualHost>"; \
} > /etc/apache2/sites-available/facade.conf

RUN set -eux; \
  # Unlink default apache configurations.
  a2dissite 000-default; \
  a2dissite default-ssl.conf; \
  a2ensite facade

WORKDIR "${FACADE_DIR}"

# Install modules.
FROM base as builder

ARG GITHUB_TOKEN="${GITHUB_TOKEN}"
ARG FACADE_VERSION="${FACADE_VERSION}"
ARG FACADE_COMPOSER_PROJECT="${FACADE_COMPOSER_PROJECT}"

RUN php -r "copy('https://getcomposer.org/installer', '/tmp/composer-setup.php');" \
  && php /tmp/composer-setup.php --install-dir /usr/local/bin --filename composer

RUN git config --global url."https://github.com/".insteadOf git@github.com: \
 && git config --global url."https://".insteadOf git://

# Run this command to avoid github API rate limiting.
RUN composer config --global github-oauth.github.com "${GITHUB_TOKEN}"
RUN composer create-project "${FACADE_COMPOSER_PROJECT}:${FACADE_VERSION}" "${FACADE_DIR}"

FROM base as facade

# Copy the directory containing modules prepared by builder.
COPY --from=builder "${FACADE_DIR}" "${FACADE_DIR}"
COPY entrypoint.sh entrypoint.sh
RUN chmod +x entrypoint.sh

EXPOSE 80

