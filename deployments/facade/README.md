# How to set up Façade on ECS infrastructure using Docker and CFn

This article documents the process of setting up Façade module on the Amazon
ECS infrastructure.

## Instructions

### Prerequisites

1. Make sure you have an ECR repository setup to push images to.
2. This CFn setups a DNS name, and Route53 for the site. You will need a
   hosted zone setup.
3. Make sure there you have two public subnets and two private subnets. RDS,
   Memcache and the Load balancers require it.
4. Make sure you have an SSL certificate for your hosted zone. You will need
   the ARN to the certificate.

### To build the docker container and use the CFn template

1. Go to [deployments/facade](../../deployments/facade).
2. In the [scripts](scripts) directory, copy the `env.template` to `.env` and
   change the parameters to fit your environment.
3. Run build.sh to build the Docker image and push it to your ECR
4. Go to CloudFormation and launch the
   [facade_ecs_elasticache_rds_prod.yml](cfn/facade_ecs_elasticache_rds_prod.yml)
5. When the site is built, you will get an email with a one-time login link to
   access the site.

---

# Amazon ECS version deployed into CDK Environment for Façade module | drupal/facade

This article documents configuring the Façade module for use with tenants
hosted on ECS (`drupal/facade`), especially focused on using
[OpenStack provider](../modules/tenants/openstack_provider).

---

## Prerequisites

The Façade module has the following prerequisites:

1. The Cloud module suite needs to be installed and enabled. The modules
   OpenStack, and AWS Cloud must be enabled.
2. API access to AWS region where Façade launches Cloud Orchestrator.
3. API access to OpenStack region that Cloud Orchestrator will manage.  This
   includes knowing the **Username/Password/Domain ID/Project ID** and
   **Region** values.  The **Project ID** must correspond with the admin
   project. If any other project id is used, Façade won’t be able to create
   users, projects and roles.
4. Cloud Orchestrator tenants are installed into a shared ECS environment build
   using the CDK. Before going through the steps below, a valid CDK environment
   and CI/CD pipeline is required.
5. Setup Amazon SES to send emails from Cloud Orchestrator tenants. Amazon SES
   is used to send out Drupal system emails such as `Forgot Password`

**NOTE:** Make note of all the CFn output from Step 4.  Those values will be
entered during Façade module setup

---

## Build Cloud Orchestrator Installation Docker Image

Orchestrator into the production CDK environment

Façade launches a CFn template that runs a docker image to install Cloud
That docker image needs to be built and pushed to an accessible ECR repository.

1. Create a new ECR repository to host this docker image.
2. Build the Docker image. Use the [build.sh](https://drupal.org/project/facade/deployments/facade/scripts/build.sh)  
   to build the image.  You can specify the ECR endpoints and image names in the
   `.env` file.  The build.sh script will push the built Docker image to that 
   repository.

**NOTE:** Make a note of the Docker image URL.  You will need to enter that
during Façade module setup.

---

## Initial Setup

Before enabling the Façade module, setup AWS and OpenStack cloud service
providers.

- Setup OpenStack cloud service provider regions that tenant users can choose.
- Setup AWS cloud service provider where Cloud Orchestrator is launched into.

In the screenshot below, there are two OpenStack regions tenant users can
select.  The AWS Oregon region is where Cloud Orchestrators are launched into.

![Cloud service providers](images/1.png)

![Admin project](images/2.png)

**NOTE:** Make sure the Project ID matches with the admin project.  Otherwise,
an error is returned by OpenStack when trying to create a user, role and
project.

---

## Enable and configure Façade module

After setting up OpenStack and AWS cloud service providers, enable the Façade
and OpenStack Provider modules.

![Cloud_Orchestrator](images/3.png)

**NOTE:**  Do not enable the **Façade remote worker** module.  This module is
designed to run on the Cloud Orchestrator instance so Façade can communicate
with it.

---

## Configure the OpenStack Provider module

Navigate to the **OpenStack provider** configuration page to configure tenant
launch options.

From the top navigation dropdown, go to
**"Configuration > Web services > Façade > OpenStack provider configuration"**, 
or directly navigate to
`/admin/config/services/facade/openstack_provider/settings`

The following screenshots describe the available parameters.  These parameters
are used to launch the CloudFormation template.  Tenant users do not need to
know anything about these.

![OpenStack provider launch parameters](images/4.png)

![OpenStack_provider launch parameters](images/5.png)

![OpenStack provider launch parameters](images/6.png)

---

## Launch a Tenant

Launch a tenant by clicking the **Tenants** link on the left hand menu.

![Tenants](images/7.png)

Fill out the **Add tenant** form.

![tenant](images/8.png)

After saving, the Tenant creation process starts.  The process takes around 20
minutes to complete.

![Demo1](images/9.png)

Email notification after Tenant creation.  Use the link in the email to login
and set up your password.

**NOTE:** Check junk folders for the email.

![AWS Systems Manager Session Manager](images/10.png)

Tenant screen after creation is completed.

**NOTE:** The **Add new project** button only shows up after a tenant is
created.

![Demo1](images/11.png)

---

## Add new project (after tenant is created)

To add a new project after tenant creation, use the **Add new project** button.

![Demo1](images/12.png)

![Add new project](images/13.png)

After creating the project, it will be added to the tenant’s Cloud Orchestrator.
It is also displayed in the **Projects** table on the Tenant page.

![Demo1](images/14.png)

---

## Share projects with other tenant user

Sharing a project with another user lets them access your project without giving
them a login to your Cloud Orchestrator instance.

Sharing a project adds a particular project to another user’s Cloud Orchestrator
instance.

![Demo1](images/15.png)

You need to know the **Username** of the tenant user you want to share the
project with.  Upon clicking the **Share project** button, the desired project
is added to the target tenant’s Cloud Orchestrator.

![Share project](images/16.png)

---

## Setup additional users for testing

During testing, it is helpful to have multiple users setup in the **Façade**
module. Having multiple users is desired when testing out the **Share**
functionality.

If setting up additional **non-administrator users** to launch tenants,
additional Façade permissions are required.  The following screenshots show
additional permissions that need to be configured. The following screenshots
show additional permissions that need to be configured.

![Role Cloud_Orchestrator-perms](images/17.png)

![Role](images/18.png)

---
