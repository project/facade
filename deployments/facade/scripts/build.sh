#!/bin/bash

ENV_FILE=$(pwd)/.env
if [ ! -e "$ENV_FILE" ]; then
  echo 'No .env file found in scripts directory.  Please create one using env.template.'
  exit 1
fi;

source ${ENV_FILE}

docker build \
  -t ${FACADE_IMAGE_NAME} \
  --build-arg GITHUB_TOKEN="${GITHUB_TOKEN}" \
  --build-arg FACADE_VERSION="${FACADE_VERSION}" \
  --build-arg FACADE_COMPOSER_PROJECT="${FACADE_COMPOSER_PROJECT}" \
  ${DOCKER_FILE_DIR}

# Login to ECR.
PASSWORD=`aws ecr get-login-password --region ${AWS_REGION}`

# Push the newly build image to ECR.
docker login --username AWS --password ${PASSWORD} ${ECR_URL}
docker tag ${CO_ECS_TAG} ${ECR_URL}/${ECR_REPO}
docker push ${ECR_URL}/${ECR_REPO}
