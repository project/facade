<?php

/**
 * @file
 * Contains tenant.page.inc.
 *
 * Page callback for Tenant entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Tenant templates.
 *
 * Default template: tenant.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_tenant(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
