# Façade

Façade is a generic framework module for managing different types of Drupal
sites.  The module provides a `Tenant` entity and a plugin manager
`FacadeLaunchTenantPluginManager`.  The plugin manger lets implementing
modules react to `Tenant` operations such as create, delete and edit.

Implementing modules can define their custom `Tenent` type and react
to `Tenant` operations with a `FacadeLaunchTenantPlugin`.

The reference implementation, `OpenStack Provider`, manages an OpenStack
specific [Cloud Orchestrator distribution](https://www.drupal.org/project/cloud_orchestrator)
using Amazon EC2 and Amazon CloudFormation.

For a full description of the module, visit the
[Façade](https://www.drupal.org/project/facade).

## Requirements

This Façade module requires no modules outside of Drupal core.

The implementing sub-module `OpenStack Provider` requires [Cloud module](https://www.drupal.org/project/cloud)

The `OpenStack Provider` module requires the following external resources.
- Working OpenStack instance(s).  Install [OpenStack DevStack](https://docs.openstack.org/devstack/latest/)
for testing the module.
- Amazon AWS account, with privileges to perform operations on EC2,
CloudFormation, VPC, RDS, IAM and Lambda.

## Installation

To try out `Façade` and the `OpenStack Provider` implementation, use composer
to create a `Cloud Orchestrator` project with the following command.

```
composer create-project \
  docomoinnovations/cloud_orchestrator:8.x-dev cloud_orchestrator
```

After the project is built, enable the `OpenStack Provider` module.  This will
enable all the required modules.

## Configuration

After setting up prerequisites, perform the following configurations.

1. Go to 'Structure > Cloud service providers > AWS Cloud' and create a
cloud service provider where Cloud Orchestrator will be launched.
2. Go to 'Structure > Cloud service providers > OpenStack' and create an
OpenStack cloud service provider. The parameters should correspond with the
`DevStack` instance.  Make sure to specify the `admin` project when entering
the `Project ID` field.
3. Go to 'Configuration > Façade > OpenStack provider configuration' and
configure the launch parameters.
4. Go to homepage, click `Tenant` on the left-hand menu and add a Tenant.  The
launch sets up a new Cloud Orchestrator on EC2 and adds OpenStack as cloud
service providers.

## Videos and slides

- YouTube: [Empower Your Single Site for Multi-tenancy Services (Stanford
  WebCamp 2023)](https://stanford.io/3q7t1Qg)
- YouTube: [Façade module demo (21'19")](https://youtu.be/tHDsE4L5AWs?t=1274)
- SlideShare: [Empower Your Single Site for Multi-tenancy Services (Stanford
  WebCamp 2023)](https://bit.ly/3CDA7Pn)

## Maintainers

- baldwinlouie - [baldwinlouie](https://www.drupal.org/u/baldwinlouie)
- yas - [yas](https://www.drupal.org/u/yas)
