<?php

/**
 * @file
 * File for update hooks.
 */

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Add shared project boolean to OpenStack Tenant Project entity.
 */
function openstack_provider_update_9101(): void {
  $db = \Drupal::database();
  $definition_update_manager = \Drupal::entityDefinitionUpdateManager();
  $entity_type = 'openstack_tenant_project';
  $fields = [];
  $fields['shared'] = BaseFieldDefinition::create('boolean')
    ->setLabel('Shared Project')
    ->setDescription('TRUE if shared project.  Defaults to FALSE')
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'type' => 'string',
      'weight' => -5,
    ])
    ->setDefaultValue(FALSE)
    ->setReadOnly(TRUE);
  foreach ($fields as $name => $definition) {
    $definition_update_manager->installFieldStorageDefinition(
      $name,
      $entity_type,
      'openstack_provider',
      $definition
    );
  }

  if ($db->schema()->fieldExists('openstack_tenant_project', 'shared')) {
    $db->update('openstack_tenant_project')
      ->fields([
        'shared' => 0,
      ])
      ->execute();
  }

  drupal_flush_all_caches();
}

/**
 * Update the default rds version to 10.5.19.
 */
function openstack_provider_update_9102(): void {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('openstack_provider.settings');
  $config->set('rds_version', '10.5.19');
  $config->save();
}

/**
 * Update the CFn template to 8.x-dev.
 */
function openstack_provider_update_9103(): void {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('openstack_provider.settings');
  $config->set('cloud_orchestrator_version', '8.x-dev');
  $config->save();
}

/**
 * Add country, city and zoom fields to Tenant entity.
 */
function openstack_provider_update_9104(): void {
  // Add new location map fields.
  \Drupal::service('cloud')->addFields(
    'tenant',
    'openstack',
    [
      'field_country',
      'field_city',
      'field_zoom_level',
    ],
    'openstack_provider'
  );

}

/**
 * Add autocreate_projects configuration.
 */
function openstack_provider_update_9105(): void {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('openstack_provider.settings');
  $config->set('autocreate_projects', FALSE);
  $config->save();
}

/**
 * Refactor configuration parameters for openstack_provider.settings.
 *
 * Delete billing field and add subdomain field.
 */
function openstack_provider_update_9106(): void {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('openstack_provider.settings');
  $config->set('rds_instance_secret_name', '');
  $config->set('ecs_cluster_arn', '');
  $config->set('image_uri', '');
  $config->set('installation_lambda_arn', '');
  $config->set('ecs_task_definition_arn', '');
  $config->set('ecs_security_group', 'facade');
  $config->set('facade_task_definition_container_name', 'facade');
  $config->set('efs_id', '');
  $config->set('efs_access_point_id', '');
  $config->set('load_balancer_dns_name', '');
  $config->set('github_token', '');
  $config->set('drupal_enduser_username', 'cloud_end_user_admin');
  $config->set('drupal_enduser_email', '');
  $config->set('use_spa_ui', 'true');
  $config->set('country', '');
  $config->set('city', '');
  $config->set('welcome_subject', 'Cloud Orchestrator is ready for use');
  $config->set('welcome_message', 'Congratulations!  Cloud Orchestrator installed successfully.  It is ready to use.\r\n\r\nPlease use this link to login and set your password.\r\n{$LOGIN_LINK}\r\n\r\nYour username is: {$CO_ADMIN_ID}.  You can access Cloud Orchestrator at: https://{$DNS_NAME}\r\n\r\nCloud Orchestrator team.');
  $config->set('enable_smtp', 'true');
  $config->set('smtp_secret_name', '');
  $config->set('memcache_address', '');
  $config->set('memcache_port', '');
  $config->set('hosted_zone_name', '');
  $config->set('lambda_timeout', '600');

  $config->clear('database_name');
  $config->clear('private_subnet_1');
  $config->clear('private_subnet_2');
  $config->clear('instance_type');
  $config->clear('db_allocated_storage');
  $config->clear('rds_instance_type');
  $config->clear('mysql_username');
  $config->clear('creation_timeout');
  $config->clear('docker_file_url');
  $config->clear('rds_version');
  $config->clear('cloud_orchestrator_version');
  $config->save();

  // Delete billing field since it is not needed.
  \Drupal::service('cloud')->deleteFields(
    'tenant',
    'openstack',
    [
      'field_billing_information',
    ]
  );

  \Drupal::service('cloud')->addFields(
    'tenant',
    'openstack',
    [
      'field_subdomain',
    ],
    'openstack_provider'
  );

}
