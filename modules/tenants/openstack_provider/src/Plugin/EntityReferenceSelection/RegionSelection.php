<?php

namespace Drupal\openstack_provider\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an entity reference plugin for selecting allowed OpenStack regions.
 *
 * @EntityReferenceSelection(
 *   id = "openstack_provider_region_selection",
 *   label = @Translation("Allowed OpenStack regions"),
 *   group = "openstack_provider_region_selection",
 *   weight = 0
 * )
 */
class RegionSelection extends SelectionPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new selection object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory interface.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $config_factory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * Get allowed openstack cloud service providers.
   */
  private function getAllowedEntities() {
    $config = $this->configFactory->get('openstack_provider.settings');
    $allowed_regions = $config->get('selectable_regions');
    if (empty($allowed_regions)) {
      return [];
    }
    return $this->entityTypeManager
      ->getStorage('cloud_config')
      ->loadMultiple($allowed_regions);
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    // Get the allowed OpenStack regions configured in the settings form.
    // The variables match,match_operator and limit are not supported
    // at this time.
    $options = [];
    $entities = $this->getAllowedEntities();
    foreach ($entities ?: [] as $entity_id => $entity) {
      $options[$entity->bundle()][$entity_id] = Html::escape($entity->label());
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function countReferenceableEntities($match = NULL, $match_operator = 'CONTAINS') {
    $entities = $this->getAllowedEntities();
    return count($entities);
  }

  /**
   * {@inheritdoc}
   */
  public function validateReferenceableEntities(array $ids) {
    // Referenceable entities are configured in the openstack_provider form.
    $config = $this->configFactory->get('openstack_provider.settings');
    return $config->get('selectable_regions');
  }

}
