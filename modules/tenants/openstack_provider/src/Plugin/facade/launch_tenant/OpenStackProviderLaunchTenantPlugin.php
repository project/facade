<?php

namespace Drupal\openstack_provider\Plugin\facade\launch_tenant;

use Drupal\aws_cloud\Entity\CloudFormation\Stack;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\facade\Entity\TenantInterface;
use Drupal\facade\Plugin\launch_tenant\FacadeLaunchTenantPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides plugin for launching OpenStack specific tenants.
 */
class OpenStackProviderLaunchTenantPlugin extends PluginBase implements FacadeLaunchTenantPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Number of supported JSON chunks.
   */
  private const JSON_LIMIT = 3;

  /**
   * Length to chunk the JSON data.
   */
  private const CHUNK_LENGTH = 4000;

  /**
   * The OpenStackProvider service.
   *
   * @var \Drupal\openstack_provider\Service\OpenStackProviderService
   */
  protected $openStackProviderService;

  /**
   * The Cloud Orchestrator Manager service.
   *
   * @var \Drupal\aws_cloud\Service\AwsCloud\AwsCloudCloudOrchestratorManager
   */
  protected $cloudOrchestratorManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
    $instance->configFactory = $container->get('config.factory');
    $instance->openStackProviderService = $container->get('openstack_provider');
    $instance->cloudOrchestratorManager = $container->get('aws_cloud.cloud_orchestrator_manager');
    $instance->logger = $container->get('logger.channel.default');
    $instance->messenger = $container->get('messenger');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function launch(TenantInterface $tenant): void {
    try {
      // Only create openstack resources if configured for autocreate.
      if ($this->configFactory->get('openstack_provider.settings')->get('autocreate_projects') === TRUE) {
        // Create OpenStack resources.
        $this->createOpenStackResources($tenant);
      }
      // Create the CFn stack.
      $this->createStack($tenant);
      // Notify the user.
      $this->messenger->addMessage($this->t('Cloud Orchestrator has launched.  You will receive an email at %email when initialization is completed.', [
        '%email' => $tenant->get('field_email')->value,
      ]));
    }
    catch (\Exception $e) {
      // If there is an exception, clean up the OpenStack resources.
      $this->openStackProviderService->cleanupOpenStackResources($tenant);
      $this->messenger->addError($this->t('Unable to continue creating Cloud Orchestrator. @exception', [
        '@exception' => $e->getMessage(),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete(TenantInterface $tenant): void {
    // During tenant deletion, delete the stack in AWS.
    $stack = $tenant->get('field_aws_cloud_stack')->entity;
    if (empty($stack)) {
      return;
    }
    $this->cloudOrchestratorManager->undeploy([$stack]);
    $this->messenger->addMessage($this->t('Cloud Orchestrator is being deleted.'));
  }

  /**
   * Helper to create the OpenStack Resources.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to launch.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createOpenStackResources(TenantInterface $tenant): void {
    $cloud_configs = $tenant->get('field_openstack_region')->referencedEntities();
    /** @var \Drupal\cloud\Entity\CloudConfig $cloud_config */
    foreach ($cloud_configs ?: [] as $cloud_config) {
      // Create resources for each selected region.
      $this->openStackProviderService->createOpenStackResource($cloud_config, $tenant, $tenant->label(), $cloud_config->label());
    }
  }

  /**
   * Helper to create the CloudFormation stack.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to launch.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Exception
   */
  private function createStack(TenantInterface $tenant): void {
    // Fetch the parameters required to launch the template
    // and get all the parameter values stored in configuration and
    // the Tenant entity.
    $parameters = $this->openStackProviderService->compileYamlParameters($this->pluginDefinition['parameter_yaml'], $tenant);

    // Only send JSON data if autocreate_projects is set to TRUE.
    if ($this->configFactory->get('openstack_provider.settings')->get('autocreate_projects') === TRUE) {
      // Add chunked region data to CFn parameters.
      $chunked_data = $this->getJsonDataForCfn($tenant);
      if (empty($chunked_data)) {
        return;
      }
      $i = 1;
      foreach ($chunked_data as $data) {
        $parameters['parameters']["JSONData{$i}"] = $data;
        $i++;
      }
    }

    $result = $this->cloudOrchestratorManager->deploy(
      $parameters['target_provider'],
      $parameters['cfn_body'],
      $parameters['parameters']
    );
    if (empty($result)) {
      throw new \Exception('Unable to deploy Cloud Orchestrator stack.');
    }
    $entity_info = array_shift($result);
    $stack = Stack::load($entity_info['entity_id']);
    // Save reference to aws_cloud_stack and store stack_id string.
    $tenant->set('field_aws_cloud_stack', $entity_info['entity_id']);
    $tenant->set('field_stack_id', $stack->getStackId() ?? '');
    $tenant->set('field_bearer_token', $parameters['parameters']['BearerToken']);
    $tenant->save();
  }

  /**
   * Helper function to gather and chunk selected regions into JSON object.
   *
   * This is a temporary solution given the assumption that CloudFormation
   * parameter values can only take up to 4096 bytes.  There are three
   * CFn parameters called JSONData1, JSONData2, JSONData3 that will
   * hold the chunked json string.
   *
   * The CFn template will use Fn::Join to rebuild the string when CFn runs.
   *
   * In the future, look into using SSM variable or some other mechanism to
   * send region data.
   *
   * See https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cloudformation-limits.html
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to launch.
   *
   * @return array
   *   JSON object chunked into smaller arrays.
   */
  private function getJsonDataForCfn(TenantInterface $tenant): array {
    try {
      $tenant_projects = $this->entityTypeManager
        ->getStorage('openstack_tenant_project')
        ->loadByProperties([
          'tenant' => $tenant->id(),
        ]);
      if (empty($tenant_projects)) {
        return [];
      }
      $tenant_objects = [];
      foreach ($tenant_projects ?: [] as $project) {
        $cloud_config = $project->get('openstack_region')->entity;
        $tenant_objects[] = [
          'name' => $cloud_config->label(),
          'type' => $cloud_config->bundle(),
          'field_identity_api_endpoint' => $cloud_config->get('field_identity_api_endpoint')->value,
          'field_identity_version' => $cloud_config->get('field_identity_version')->value,
          'field_compute_api_endpoint' => $cloud_config->get('field_compute_api_endpoint')->value,
          'field_compute_version' => $cloud_config->get('field_compute_version')->value,
          'field_image_api_endpoint' => $cloud_config->get('field_image_api_endpoint')->value,
          'field_image_version' => $cloud_config->get('field_image_version')->value,
          'field_network_api_endpoint' => $cloud_config->get('field_network_api_endpoint')->value,
          'field_network_version' => $cloud_config->get('field_network_version')->value,
          'field_volume_api_endpoint' => $cloud_config->get('field_volume_api_endpoint')->value,
          'field_volume_version' => $cloud_config->get('field_volume_version')->value,
          'field_orchestration_api_endpoint' => $cloud_config->get('field_orchestration_api_endpoint')->value,
          'field_orchestration_version' => $cloud_config->get('field_orchestration_version')->value,
          'field_username' => $project->get('username')->value,
          'field_password' => $project->get('password')->value,
          'field_domain_id' => $project->get('domain_id')->value,
          'field_project_id' => $project->get('project_id')->value,
          'field_os_region' => $cloud_config->get('field_os_region')->value,
        ];
      }
      $obj_str = json_encode($tenant_objects, JSON_THROW_ON_ERROR);

      // Split the json object into 4000 byte chunks for passing to CFn.
      $chunked = str_split($obj_str, self::CHUNK_LENGTH);
      if (count($chunked) > self::JSON_LIMIT) {
        $this->messenger->addError($this->t('Too many OpenStack regions to support.'));
        return [];
      }
      return $chunked;
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Unable to build OpenStack region data for Cloud Orchestrator. @exception', [
        '@exception' => $e->getMessage(),
      ]));
      return [];
    }
  }

}
