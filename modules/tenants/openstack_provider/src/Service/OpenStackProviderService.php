<?php

namespace Drupal\openstack_provider\Service;

use Drupal\aws_cloud\Entity\CloudFormation\Stack;
use Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface;
use Drupal\cloud\Entity\CloudConfig;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Uuid\Php;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\facade\Entity\TenantInterface;
use Drupal\openstack\Service\OpenStackServiceFactoryInterface;
use Drupal\openstack_provider\Entity\OpenStackTenantProject;
use Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Service providing generic methods for openstack_provider.
 */
class OpenStackProviderService implements OpenStackProviderServiceInterface, ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Description string to send to OpenStack.
   */
  public const DESCRIPTION_STRING = 'auto-created-from-facade';

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $extensionPathResolver;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The password generator service.
   *
   * @var \Drupal\openstack_provider\Service\OpenStackPasswordGenerator
   */
  private $passwordGenerator;

  /**
   * The AWS Cloud EC2 service.
   *
   * @var \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface
   */
  private $ec2Service;

  /**
   * The OpenStack Service Factory.
   *
   * @var \Drupal\openstack\Service\OpenStackServiceFactoryInterface
   */
  private $openStackServiceFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * The datetime interface.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private $dateTime;

  /**
   * The uuid service.
   *
   * @var \Drupal\Component\Uuid\Php
   */
  private $uuid;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager interface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system interface.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler interface.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   *   Extension path resolver.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger object.
   * @param \Drupal\openstack\Service\OpenStackServiceFactoryInterface $openstack_service_factory_interface
   *   The OpenStack factory interface.
   * @param \Drupal\aws_cloud\Service\Ec2\Ec2ServiceInterface $ec2_service
   *   The Ec2 service.
   * @param \Drupal\openstack_provider\Service\OpenStackPasswordGenerator $password_generator
   *   The password generator.
   * @param \Drupal\Component\Datetime\TimeInterface $date_time
   *   The time object.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Component\Uuid\Php $uuid
   *   The uuid service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle Http client.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    FileSystemInterface $file_system,
    ModuleHandlerInterface $module_handler,
    ExtensionPathResolver $extension_path_resolver,
    Messenger $messenger,
    OpenStackServiceFactoryInterface $openstack_service_factory_interface,
    Ec2ServiceInterface $ec2_service,
    OpenStackPasswordGenerator $password_generator,
    TimeInterface $date_time,
    AccountProxyInterface $current_user,
    Php $uuid,
    ClientInterface $http_client,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;
    $this->extensionPathResolver = $extension_path_resolver;
    $this->messenger = $messenger;
    $this->openStackServiceFactory = $openstack_service_factory_interface;
    $this->ec2Service = $ec2_service;
    $this->passwordGenerator = $password_generator;
    $this->currentUser = $current_user;
    $this->dateTime = $date_time;
    $this->uuid = $uuid;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('file_system'),
      $container->get('module_handler'),
      $container->get('extension.path.resolver'),
      $container->get('messenger'),
      $container->get('openstack.factory'),
      $container->get('openstack.ec2'),
      $container->get('password_generator'),
      $container->get('datetime.time'),
      $container->get('current_user'),
      $container->get('uuid'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function compileYamlParameters(string $yaml, EntityInterface $entity): array {

    $yaml = $this->decodeYaml($yaml);
    $parsed_parameters = [];
    if (empty($yaml['parameters'])) {
      $this->messenger->addError($this->t('Unable to retrieve parameters.'));
      return [];
    }
    foreach ($yaml['parameters'] ?: [] as $key => $parameter) {
      switch ($parameter['type']) {
        case 'config':
          $parsed_parameters[$key] = $this->getConfigurationValue($parameter['config_name'], $parameter['config_key']);
          break;

        case 'auto':
          $method = $parameter['method'];
          if (!method_exists($this, $method)) {
            break;
          }
          $parsed_parameters[$key] = $this->$method($entity);
          break;

        case 'entity':
          $parsed_parameters[$key] = $this->getEntityValue($entity, $parameter['entity_type'], $parameter['entity_bundle'], $parameter['field']);
          break;

        default:
          break;
      }
    }
    // @todo Check if any parameters are empty and halt the launch.
    return [
      'cfn_body' => file_get_contents($this->getRealPath($yaml['cloud_formation_template'])),
      'parameters' => $parsed_parameters,
      'target_provider' => $this->getConfigurationValue('openstack_provider.settings', 'target_provider'),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function createRemoteCloudConfig(TenantInterface $tenant, array $params): void {
    $bearer_token = $tenant->get('field_bearer_token')->value;
    if (empty($bearer_token)) {
      throw new \Exception('No bearer token found.');
    }
    $headers = $this->generateRestHeader($bearer_token);
    /** @var \Drupal\aws_cloud\Entity\CloudFormation\Stack $stack */
    $stack = $tenant->get('field_aws_cloud_stack')->entity;
    if (empty($stack)) {
      throw new \Exception('Cannot add project. No stack found.');
    }
    $endpoint = $this->getCloudOrchestratorUrl($stack);
    if (empty($endpoint)) {
      throw new \Exception('Cannot add project. No endpoint found.');
    }

    $options = [
      'body' => json_encode($params, JSON_THROW_ON_ERROR),
      'headers' => $headers,
    ];
    // Only set verify=FALSE if the the disable_ssl_verify is set to true.
    if ((bool) $this->getConfigurationValue('openstack_provider.settings', 'disable_ssl_verify') === TRUE) {
      $options['verify'] = FALSE;
    }
    $response = $this->httpClient->post(
      $endpoint . OpenStackProviderServiceInterface::CLOUD_ORCHESTRATOR_ADD_PROVIDER_ENDPOINT,
      $options
    );
    if ($response->getStatusCode() !== 201) {
      throw new \Exception("Error adding project to $endpoint");
    }
  }

  /**
   * {@inheritdoc}
   */
  public function cleanupOpenStackResources(TenantInterface $tenant): void {
    // Loop through the tenant projects.  Delete all the related resources.
    $projects = $this->entityTypeManager->getStorage('openstack_tenant_project')
      ->loadByProperties(
        [
          'tenant' => $tenant->id(),
          'shared' => FALSE,
        ]);
    foreach ($projects ?: [] as $project) {
      $this->cleanupUserCreatedResources($project);
      $this->deleteOpenStackResource($project);
    }
  }

  /**
   * Clean up resources created by the user.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function cleanupUserCreatedResources(OpenStackTenantProject $project): void {
    // NOTE: The sequence of deletes is important.  For example, network
    // cannot be deleted until all the ports, subnets and routers are delete.
    // $this->purgeNetwork() is further down from the rest of the network
    // entities so OpenStack has time to process those entities first.
    // Otherwise, networks won't be deleted.  This is the general order of
    // entities to delete so there are no deletion errors.
    $this->prepEc2Service($project);
    $this->purgeFloatingIps($project);
    $this->purgeRouters($project);
    $this->purgePorts($project);
    $this->purgeSubnets($project);
    $this->purgeSecurityGroups($project);
    $this->purgeSnapshots($project);
    $this->purgeNetworks($project);
    $this->purgeKeyPairs($project);
    $this->purgeServerGroups($project);
    $this->purgeImages($project);
    $this->purgeVolumes($project);
    $this->purgeInstance($project);
  }

  /**
   * Delete the OpenStack project, user and role.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function deleteOpenStackResource(OpenStackTenantProject $project): void {
    // Get a fresh ec2Service service object to access the admin project.
    $cloud_config = $project->get('openstack_region')->entity;
    $this->ec2Service = $this->openStackServiceFactory->get($cloud_config->getCloudContext());
    $this->deleteOpenStackUser($project->getUserId());
    $this->deleteOpenStackRole($project->getRoleId());
    $this->deleteOpenStackProject($project->getProjectId());
  }

  /**
   * {@inheritdoc}
   */
  public function createOpenStackResource(CloudConfig $cloud_config, TenantInterface $tenant, string $resource_prefix, string $project_entity_name): OpenStackTenantProjectInterface {
    $ec2_service = $this->openStackServiceFactory->get($cloud_config->getCloudContext());
    $this->ec2Service = $ec2_service;

    // Fetch the domain_id.
    $domain_id = $cloud_config->get('field_domain_id')->value;

    // Setup some defaults values to pass to the APIs.
    $project_name = $this->generateNameString($resource_prefix, 'project');
    $username = $this->generateNameString($resource_prefix, 'user');
    $role_name = $this->generateNameString($resource_prefix, 'role');
    $password = $this->passwordGenerator->generate(12);

    $project_id = $this->createOpenStackProject($domain_id, $project_name);
    $role_id = $this->createOpenStackRole($role_name);
    $user_id = $this->createOpenStackUser($username, $password, $tenant->get('field_email')->value);
    $this->assignRoleToUserOnProject($project_id, $user_id, $role_id);
    $this->assignAdditionalRolesToUsers($project_id, $user_id, $cloud_config->getCloudContext());

    // Add an OpenStackTenantProject entity with all the generated info.
    $tenant_project_entity = OpenStackTenantProject::create([
      'name' => $project_entity_name,
      'created' => $this->dateTime->getRequestTime(),
      'uid' => $this->currentUser->id(),
      'tenant' => $tenant->id(),
      'openstack_region' => $cloud_config->id(),
      'username' => $username,
      'password' => $password,
      'project_id' => $project_id,
      'domain_id' => $domain_id,
      'role_id' => $role_id,
      'user_id' => $user_id,
      'project_name' => $project_name,
      'role_name' => $role_name,
    ]);
    $tenant_project_entity->save();
    return $tenant_project_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteCloudConfigByName(TenantInterface $tenant, string $name): array {
    $bearer_token = $tenant->get('field_bearer_token')->value;
    if (empty($bearer_token)) {
      throw new \Exception('No bearer token found.');
    }
    $headers = $this->generateRestHeader($bearer_token);

    $stack = $tenant->get('field_aws_cloud_stack')->entity;
    if (empty($stack)) {
      throw new \Exception('No AWS cloud stack found.');
    }
    $endpoint = $this->getCloudOrchestratorUrl($stack);
    if (empty($endpoint)) {
      throw new \Exception('No endpoint found');
    }
    $data = [];
    try {
      $options = [
        'headers' => $headers,
        'query' => [
          'name' => $name,
        ],
      ];
      // Only set verify=FALSE if the the disable_ssl_verify is set to true.
      if ((bool) $this->getConfigurationValue('openstack_provider.settings', 'disable_ssl_verify') === TRUE) {
        $options['verify'] = FALSE;
      }
      $response = $this->httpClient->get(
        $endpoint . OpenStackProviderServiceInterface::CLOUD_ORCHESTRATOR_GET_PROVIDER_ENDPOINT,
        $options
      );
      if ($response->getStatusCode() !== 200) {
        throw new \Exception('Error accessing cloud orchestrator.');
      }
      $results = $response->getBody()->getContents();
      $data = json_decode($results, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    catch (\Exception $e) {
      throw new \Exception($e->getMessage());
    }
    catch (GuzzleException $ge) {
      throw new \Exception($ge->getMessage());
    }
    return $data;
  }

  /**
   * Helper to get the DrupalUrl from the CFn output.
   *
   * @param \Drupal\aws_cloud\Entity\CloudFormation\Stack $stack
   *   The CloudFormation stack object.
   *
   * @return string|null
   *   The endpoint url or null.
   */
  private function getCloudOrchestratorUrl(Stack $stack): ?string {
    $output = $stack->getOutputs();
    $array_index = array_search(OpenStackProviderServiceInterface::CLOUD_ORCHESTRATOR_URL_KEY, array_column($output, 'item_key'), TRUE);
    return empty($array_index) ? NULL : $output[(int) $array_index]['item_value'];
  }

  /**
   * Helper to generate the REST api header.
   *
   * @param string $token
   *   The token to send to the remote Cloud Orchestrator.
   *
   * @return array
   *   Header array.
   */
  private function generateRestHeader(string $token): array {
    return [
      'Authorization' => 'Bearer ' . base64_encode($token),
      'Content-Type' => 'application/json',
    ];
  }

  /**
   * Generate a unique string for OpenStack api calls.
   *
   * @param string $prefix
   *   String prefix.
   * @param string $type
   *   Entity type.
   *
   * @return string
   *   Generated string.
   */
  private function generateNameString(string $prefix, string $type): string {
    return $this->t('@prefix-@type-@unique', [
      '@prefix' => $this->formatName($prefix),
      '@type' => $type,
      '@unique' => $this->generateUniqueSuffix(),
    ])->render();
  }

  /**
   * Helper to generate unique suffix.
   *
   * Currently, timestamp is used as the unique identified, but this can change
   * in the future.
   *
   * @return string
   *   The unique identifier.
   */
  private function generateUniqueSuffix(): string {
    return time();
  }

  /**
   * Helper to create the OpenStack project.
   *
   * @param string $domain_id
   *   The domain_id.
   * @param string $project_name
   *   The project name.
   *
   * @return string|null
   *   OpenStack project id or null if there was an error.
   */
  private function createOpenStackProject(string $domain_id, string $project_name): ?string {
    $project = $this->ec2Service->createProject([
      'DomainId' => $domain_id,
      'Name' => $project_name,
      'Description' => self::DESCRIPTION_STRING,
      'Enabled' => TRUE,
    ]);
    if (empty($project)) {
      throw new \Exception('Error creating OpenStack project.');
    }
    return $project['Project']['Id'];
  }

  /**
   * Delete OpenStack project.
   *
   * @param string $project_id
   *   Project id to delete.
   */
  private function deleteOpenStackProject(string $project_id): void {
    $this->ec2Service->deleteProject([
      'ProjectId' => $project_id,
    ]);
  }

  /**
   * Delete OpenStack role.
   *
   * @param string $role_id
   *   Role id to delete.
   */
  private function deleteOpenStackRole(string $role_id): void {
    $this->ec2Service->deleteRole([
      'RoleId' => $role_id,
    ]);
  }

  /**
   * Helper to create the OpenStack role.
   *
   * @param string $role_name
   *   The role name.
   *
   * @return string|null
   *   OpenStack role id or null if there was an error.
   */
  private function createOpenStackRole(string $role_name): ?string {
    $role = $this->ec2Service->createRole([
      'Name' => $role_name,
      'Description' => self::DESCRIPTION_STRING,
    ]);
    if (empty($role)) {
      throw new \Exception('Error creating OpenStack role.');
    }
    return $role['Role']['Id'];
  }

  /**
   * Delete OpenStack user.
   *
   * @param string $user_id
   *   User id to delete.
   */
  private function deleteOpenStackUser(string $user_id): void {
    $this->ec2Service->deleteUser([
      'UserId' => $user_id,
    ]);
  }

  /**
   * Helper to create the OpenStack user.
   *
   * @param string $user_name
   *   The username.
   * @param string $password
   *   Password string.
   * @param string $email
   *   The email string.
   *
   * @return string|null
   *   OpenStack role id or null if there was an error.
   */
  private function createOpenStackUser(string $user_name, string $password, string $email): ?string {
    $user = $this->ec2Service->createUser([
      'Description' => self::DESCRIPTION_STRING,
      'Name' => $user_name,
      'Password' => $password,
      'Email' => $email,
    ]);
    if (empty($user)) {
      throw new \Exception('Error creating OpenStack user.');
    }
    return $user['User']['Id'];
  }

  /**
   * Assign a user to a project.
   *
   * @param string $project_id
   *   The project id.
   * @param string $user_id
   *   The user id.
   * @param string $role_id
   *   The role id.
   */
  private function assignRoleToUserOnProject(string $project_id, string $user_id, string $role_id): void {
    $this->ec2Service->assignRoleToUserOnProject([
      'ProjectId' => $project_id,
      'RoleId' => $role_id,
      'UserId' => $user_id,
    ]);
  }

  /**
   * Assign a user to additional roles.
   *
   * @param string $project_id
   *   The project id.
   * @param string $user_id
   *   The user id.
   * @param string $cloud_context
   *   The cloud_context the role is in.
   */
  private function assignAdditionalRolesToUsers(string $project_id, string $user_id, string $cloud_context): void {
    $additional_roles = $this->getConfigurationValue('openstack_provider.settings', 'additional_roles');
    if (empty($additional_roles)) {
      return;
    }
    $role_names = explode(',', $additional_roles);
    foreach ($role_names ?: [] as $role_name) {
      $role = $this->getOpenStackRole($role_name, $cloud_context);
      if (empty($role)) {
        continue;
      }
      $this->assignRoleToUserOnProject($project_id, $user_id, $role->getRoleId());
    }
  }

  /**
   * Get an OpenStack role entity given the OpenStack name.
   *
   * @param string $name
   *   Name of entity to retrieve.
   * @param string $cloud_context
   *   The cloud_context to retrieve.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity for the OpenStack role
   */
  private function getOpenStackRole(string $name, string $cloud_context): ?EntityInterface {
    if (empty($name)) {
      return NULL;
    }

    try {
      $roles = $this->entityTypeManager
        ->getStorage('openstack_role')
        ->loadByProperties([
          'cloud_context' => $cloud_context,
          'name' => trim($name),
        ]);

      if (!empty($roles)) {
        // Return the first entity.
        return reset($roles);
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function generateStackName(EntityInterface $entity): string {
    return $this->t(':name-:timestamp', [
      ':name' => $this->formatName($entity->label()),
      ':timestamp' => time(),
    ])->render();
  }

  /**
   * {@inheritdoc}
   */
  public function generateStackPrefix(EntityInterface $entity): string {
    // Stack prefix can only be 20 characters in length.
    return $this->t(':name-:timestamp', [
      ':name' => substr($this->formatName($entity->label()), 0, 9),
      ':timestamp' => time(),
    ])->render();
  }

  /**
   * {@inheritdoc}
   */
  public function generateDatabaseName(EntityInterface $entity): string {
    $subdomain = $entity->get('field_subdomain')->value;
    return $this->t(':subdomain_:timestamp_co', [
      ':subdomain' => substr($this->formatName($subdomain), 0, 15),
      ':timestamp' => time(),
    ])->render();
  }

  /**
   * Helper function to strip tenant entity name.
   *
   * The formatted tenant name is used for CloudFormation stack name and
   * can only contain alphanumeric characters and hyphens.
   *
   * @param string $name
   *   Name to format.
   *
   * @return string
   *   Formatted name.
   */
  private function formatName(string $name): string {
    $name = preg_replace('/[^a-zA-Z0-9]/', '', $name);
    return strtolower($name);
  }

  /**
   * Generate bearer token.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Tenant entity.
   *
   * @return string
   *   The generated bearer token.
   */
  public function generateBearerToken(EntityInterface $entity): string {
    return $this->uuid->generate();
  }

  /**
   * Takes the OpenStack region and joins it with the project credentials.
   *
   * This gives access to a tenant user's resources.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   *
   * @return array
   *   Credential array for accessing OpenStack API.
   */
  private function buildCredentials(OpenStackTenantProject $project): array {
    $cloud_config = $project->get('openstack_region')->entity;
    return [
      'region'                     => $cloud_config->get('field_os_region')->value,
      'self_signed_cert_path'      => $cloud_config->get('field_self_signed_cert_path')->value,
      'identity_api_endpoint'      => $cloud_config->get('field_identity_api_endpoint')->value,
      'identity_version'           => $cloud_config->get('field_identity_version')->value,
      'compute_api_endpoint'       => $cloud_config->get('field_compute_api_endpoint')->value,
      'compute_version'            => $cloud_config->get('field_compute_version')->value,
      'image_api_endpoint'         => $cloud_config->get('field_image_api_endpoint')->value,
      'image_version'              => $cloud_config->get('field_image_version')->value,
      'network_api_endpoint'       => $cloud_config->get('field_network_api_endpoint')->value,
      'network_version'            => $cloud_config->get('field_network_version')->value,
      'volume_api_endpoint'        => $cloud_config->get('field_volume_api_endpoint')->value,
      'volume_version'             => $cloud_config->get('field_volume_version')->value,
      'orchestration_api_endpoint' => $cloud_config->get('field_orchestration_api_endpoint')->value,
      'orchestration_version'      => $cloud_config->get('field_orchestration_version')->value,
      'user_id' => $project->getUserId(),
      'username' => $project->getUsername(),
      'password' => $project->getPassword(),
      'domain_id' => $project->getDomainId(),
      'project_id' => $project->getProjectId(),
    ];
  }

  /**
   * Helper method to prep the Ec2Service class for accessing user resources.
   *
   * Use the credentials stored in the OpenStackTenantProject and build a
   * credential array.  The credential array is passed to OpenStackService
   * to access a tenant's resources without adding the Admin user to their
   * project.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function prepEc2Service(OpenStackTenantProject $project): void {
    $credentials = $this->buildCredentials($project);
    $cloud_config = $project->get('openstack_region')->entity;
    $this->ec2Service = $this->openStackServiceFactory->get($cloud_config->getCloudContext());
    $this->ec2Service->setCredentials($credentials);
  }

  /**
   * Purge instances.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeInstance(OpenStackTenantProject $project): void {
    $reservations = $this->ec2Service->describeInstances([
      'project_id' => $project->getProjectId(),
    ]);
    foreach ($reservations['Reservations'] ?: [] as $reservation) {
      foreach ($reservation['Instances'] ?: [] as $instance) {
        $this->ec2Service->terminateInstances([
          'InstanceId' => $instance['InstanceId'],
        ]);
      }
    }
  }

  /**
   * Purge images.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeImages(OpenStackTenantProject $project): void {
    $images = $this->ec2Service->describeImages();
    foreach ($images['Images'] ?: [] as $image) {
      $this->ec2Service->deregisterImage([
        'ImageId' => $image['ImageId'],
      ]);
    }
  }

  /**
   * Purge server groups.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeServerGroups(OpenStackTenantProject $project): void {
    $server_groups = $this->ec2Service->describeServerGroups();
    foreach ($server_groups['ServerGroups'] ?: [] as $server_group) {
      $this->ec2Service->deleteServerGroup([
        'ServerGroupId' => $server_group['ServerGroupId'],
      ]);
    }
  }

  /**
   * Purge keypairs.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeKeyPairs(OpenStackTenantProject $project): void {
    $key_pairs = $this->ec2Service->describeKeyPairs();
    foreach ($key_pairs['KeyPairs'] ?: [] as $key_pair) {
      $this->ec2Service->deleteKeyPair([
        'KeyName' => $key_pair['KeyName'],
      ]);
    }
  }

  /**
   * Purge snapshots.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeSecurityGroups(OpenStackTenantProject $project): void {
    $security_groups = $this->ec2Service->describeSecurityGroups([
      'project_id' => $project->getProjectId(),
    ]);
    foreach ($security_groups['SecurityGroups'] ?: [] as $security_group) {
      // Skip default security group.
      if ($security_group['GroupName'] === 'default') {
        continue;
      }
      $this->ec2Service->deleteSecurityGroup([
        'GroupId' => $security_group['GroupId'],
      ]);
    }
  }

  /**
   * Purge snapshots.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeSnapshots(OpenStackTenantProject $project): void {
    $snapshots = $this->ec2Service->describeSnapshots();
    foreach ($snapshots['Snapshots'] ?: [] as $snapshot) {
      $this->ec2Service->deleteSnapshot([
        'SnapshotId' => $snapshot['SnapshotId'],
      ]);
    }
  }

  /**
   * Purge volumes.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeVolumes(OpenStackTenantProject $project): void {
    $volumes = $this->ec2Service->describeVolumes([
      'project_id' => $project->getProjectId(),
    ]);

    foreach ($volumes['Volumes'] ?: [] as $volume) {
      // Detach volumes before deleting.
      if ($volume['State'] === 'in-use') {
        // Loop through attachment array, and try to detach the volume
        // before deleting it.
        foreach ($volume['Attachments'] ?: [] as $attachment) {
          $this->ec2Service->detachVolume([
            'VolumeId' => $attachment['VolumeId'],
          ]);
        }
      }
      $this->ec2Service->deleteVolume([
        'VolumeId' => $volume['VolumeId'],
      ]);
    }
  }

  /**
   * Purge networks.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeNetworks(OpenStackTenantProject $project): void {
    $networks = $this->ec2Service->describeNetworks([
      'project_id' => $project->getProjectId(),
    ]);
    foreach ($networks['Networks'] ?: [] as $network) {
      if ($network['ProjectId'] !== $project->getProjectId()) {
        continue;
      }
      $this->ec2Service->deleteNetwork([
        'NetworkId' => $network['NetworkId'],
      ]);
    }
  }

  /**
   * Purge subnets.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeSubnets(OpenStackTenantProject $project): void {
    $subnets = $this->ec2Service->describeSubnets([
      'project_id' => $project->getProjectId(),
      '_ignore_network' => TRUE,
    ]);
    foreach ($subnets['Subnets'] ?: [] as $subnet) {
      // Don't attempt to delete subnets that are not part of the project.
      if ($subnet['ProjectId'] !== $project->getProjectId()) {
        continue;
      }
      $this->ec2Service->deleteSubnet([
        'SubnetId' => $subnet['SubnetId'],
      ]);
    }
  }

  /**
   * Purge floating ips.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeFloatingIps(OpenStackTenantProject $project): void {
    $ips = $this->ec2Service->describeAddresses();
    foreach ($ips['Addresses'] ?: [] as $ip) {
      $this->ec2Service->releaseAddress([
        'FloatingIpId' => $ip['FloatingIpId'],
      ]);
    }
  }

  /**
   * Purge ports.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgePorts(OpenStackTenantProject $project): void {
    $ports = $this->ec2Service->describePorts([
      'project_id' => $project->getProjectId(),
      '_ignore_network' => TRUE,
    ]);
    foreach ($ports['Ports'] ?: [] as $port) {
      // Don't delete ports not associated with this project.
      if ($port['ProjectId'] !== $project->getProjectId()) {
        continue;
      }
      $this->ec2Service->deletePort([
        'PortId' => $port['PortId'],
      ]);
    }
  }

  /**
   * Purge routers.
   *
   * @param \Drupal\openstack_provider\Entity\OpenStackTenantProject $project
   *   The project entity.
   */
  private function purgeRouters(OpenStackTenantProject $project): void {
    $routers = $this->ec2Service->describeRouters([
      'project_id' => $project->getProjectId(),
    ]);
    foreach ($routers['Routers'] ?: [] as $router) {
      $this->ec2Service->deleteRouter([
        'RouterId' => $router['RouterId'],
      ]);
    }
  }

  /**
   * Get entity value given an entity field name.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to retrieve from.
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_bundle
   *   The entity bundle.
   * @param string $field
   *   Field to retrieve.
   *
   * @return string|null
   *   field value or null.
   */
  private function getEntityValue(EntityInterface $entity, string $entity_type, string $entity_bundle, string $field): ?string {
    if ($entity->getEntityTypeId() !== $entity_type && $entity->bundle() !== $entity_bundle) {
      return NULL;
    }
    return $entity->get($field)->value ?? NULL;
  }

  /**
   * Helper method to retrieve value from a specific configuration.
   *
   * @param string $config_name
   *   Configuration name.
   * @param string $config_key
   *   Configuration key.
   *
   * @return string|null
   *   Configuration value or null.
   */
  private function getConfigurationValue(string $config_name, string $config_key): ?string {
    return $this->configFactory->get($config_name)
      ->get($config_key);
  }

  /**
   * Given relative path of yaml file, resolve to absolute file path.
   *
   * @param string $path
   *   Path relative to openstack_provider module.
   *
   * @return string
   *   Absolute file path.
   */
  private function getRealPath($path): string {
    return realpath($this->extensionPathResolver->getPath('module', 'openstack_provider')) . '/' . $path;
  }

  /**
   * Decode the yaml file.
   *
   * @param string $path
   *   Yaml file to decode.
   *
   * @return array
   *   Full path to yaml file.
   */
  private function decodeYaml(string $path): array {
    $path = $this->getRealPath($path);
    return Yaml::decode(file_get_contents($path));
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationMapZoomLevels(): array {
    return [
      '1' => $this->t('1'),
      '2' => $this->t('2'),
      '3' => $this->t('3'),
      '4' => $this->t('4'),
      '5' => $this->t('5'),
      '6' => $this->t('6'),
      '7' => $this->t('7'),
      '8' => $this->t('8'),
      '9' => $this->t('9'),
    ];
  }

}
