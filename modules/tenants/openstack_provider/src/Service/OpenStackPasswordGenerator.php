<?php

namespace Drupal\openstack_provider\Service;

use Drupal\Core\Password\DefaultPasswordGenerator;
use Drupal\Core\Password\PasswordGeneratorInterface;

/**
 * Provides a cloud-specific password generator.
 */
class OpenStackPasswordGenerator extends DefaultPasswordGenerator implements PasswordGeneratorInterface {

  /**
   * The allowed characters for the password in the cloud-specific class.
   *
   * @var string
   */
  protected $allowedCharacters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()';

}
