<?php

namespace Drupal\openstack_provider\Service;

use Drupal\cloud\Entity\CloudConfig;
use Drupal\Core\Entity\EntityInterface;
use Drupal\facade\Entity\TenantInterface;
use Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface;

/**
 * OpenStack Provider interface.
 */
interface OpenStackProviderServiceInterface {

  /**
   * Cloud orchestrator url key from AWS CloudFormation stack.
   *
   * @var string
   */
  public const CLOUD_ORCHESTRATOR_URL_KEY = 'DrupalUrl';

  /**
   * Cloud orchestrator endpoint for querying cloud service providers.
   *
   * @var string
   */
  public const CLOUD_ORCHESTRATOR_GET_PROVIDER_ENDPOINT = '/facade_remote_worker/cloud_service_provider';

  /**
   * Cloud orchestrator endpoint for adding cloud service providers.
   *
   * @var string
   */
  public const CLOUD_ORCHESTRATOR_ADD_PROVIDER_ENDPOINT = '/entity/cloud_config';

  /**
   * Takes a yaml file location, parses it and fetches the values.
   *
   * @param string $yaml
   *   Yaml file location relative to openstack_provider module.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity interface.
   *
   * @return array
   *   Array of values, ready to pass to AWS CloudFormation.
   */
  public function compileYamlParameters(string $yaml, EntityInterface $entity): array;

  /**
   * Helper to create the OpenStack Resources.
   *
   * @param \Drupal\cloud\Entity\CloudConfig $cloud_config
   *   Cloud config entity.
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to launch.
   * @param string $resource_prefix
   *   Prefix used for resource creation.
   * @param string $project_entity_name
   *   Name for creating OpenStackTenantProject entity.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The created OpenStack tenant project entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createOpenStackResource(CloudConfig $cloud_config, TenantInterface $tenant, string $resource_prefix, string $project_entity_name): OpenStackTenantProjectInterface;

  /**
   * Create a remote cloud config entity from a Tenant entity.
   *
   * The params array is a serialized CloudConfig entity with the correct
   * project_id, username and password to access a specific OpenStack Project.
   * See `OpenStackAddNewTenantProjectForm::submitForm()` for an example on
   * how to serialize the CloudConfig entity.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The target tenant to create the CloudConfig for.
   * @param array $params
   *   The cloud_config array.
   *
   * @throws \Exception
   *   Throw exception if there is an error.
   */
  public function createRemoteCloudConfig(TenantInterface $tenant, array $params): void;

  /**
   * Clean up all OpenStack resources.
   *
   * Deletes all tenant user resources such as instances, networks and volumes.
   * Also deletes the actual OpenStack project, user and role.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to clean up with.
   */
  public function cleanupOpenStackResources(TenantInterface $tenant): void;

  /**
   * Search for a remote cloud config given a certain name.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The target tenant to create the CloudConfig for.
   * @param string $name
   *   Name to search.
   *
   * @return array
   *   Array of json objects.
   *
   * @throws \Exception
   *   Throw exception if there is an error.
   */
  public function getRemoteCloudConfigByName(TenantInterface $tenant, string $name): array;

  /**
   * Return an array of map zoom levels.
   *
   * @return array
   *   Array of zoom levels.
   */
  public function getLocationMapZoomLevels(): array;

  /**
   * Generate the CFn stack name.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Tenant entity.
   *
   * @return string
   *   The generated stack name.
   */
  public function generateStackName(EntityInterface $entity): string;

  /**
   * Generate the CFn stack prefix.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Tenant entity.
   *
   * @return string
   *   The generated stack name.
   */
  public function generateStackPrefix(EntityInterface $entity): string;

  /**
   * Generate the database name.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Tenant entity.
   *
   * @return string
   *   The generated database name.
   */
  public function generateDatabaseName(EntityInterface $entity): string;

}
