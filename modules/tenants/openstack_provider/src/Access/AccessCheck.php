<?php

namespace Drupal\openstack_provider\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\facade\Entity\Tenant;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Access check class for openstack_provider module.
 */
class AccessCheck implements ContainerInjectionInterface {

  /**
   * Route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match interface.
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   Current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(RouteMatchInterface $route_match, AccountProxyInterface $user, EntityTypeManagerInterface $entity_type_manager) {
    $this->routeMatch = $route_match;
    $this->user = $user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Check access to add new openstack tenant project.
   *
   * Check if the tenant->bundle() == openstack, then check if the stack
   * is created.  Finally, check any permissions defined in the routing file.
   *
   * @param \Drupal\facade\Entity\Tenant $tenant
   *   Tenant entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account to check.
   * @param \Symfony\Component\Routing\Route $route
   *   Current route.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkAddNewProjectAccess(Tenant $tenant, AccountInterface $account, Route $route): AccessResultInterface {
    if ($tenant->bundle() !== 'openstack' || $tenant->hasField('field_aws_cloud_stack') === FALSE) {
      return AccessResult::neutral();
    }
    /** @var \Drupal\aws_cloud\Entity\CloudFormation\Stack $stack */
    $stack = $tenant->get('field_aws_cloud_stack')->entity;

    if (empty($stack) || $stack->getStackStatus() !== 'CREATE_COMPLETE') {
      return AccessResult::neutral();
    }
    return $this->access($account, $route);
  }

  /**
   * Checks user access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route object.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route): AccessResultInterface {

    // Validation of the local Drupal permissions.
    $perm = $route->getOption('perm');

    // Allow to conjunct the permissions with OR ('+') or AND (',').
    $split = explode(',', $perm);
    // Support AND permission check.
    if (count($split) > 1) {
      return AccessResult::allowedIfHasPermissions($account, $split, 'AND');
    }
    else {
      $split = explode('+', $perm);
      return AccessResult::allowedIfHasPermissions($account, $split, 'OR');
    }

  }

}
