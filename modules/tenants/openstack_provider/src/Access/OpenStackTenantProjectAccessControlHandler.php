<?php

namespace Drupal\openstack_provider\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Project entity.
 *
 * @see \Drupal\openstack_provider\Entity\OpenStackTenantProject.
 */
class OpenStackTenantProjectAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface $entity */

    switch ($operation) {

      case 'delete':
      case 'view':
      case 'update':
        // Check own access.
        $permission = $this->checkAnyOwnAccess($entity, $operation, $account, TRUE);

        if (!empty($permission)) {
          return AccessResult::allowed();
        }

        // Check any access.
        $permission = $this->checkAnyOwnAccess($entity, $operation, $account, FALSE);
        if (!empty($permission)) {
          return AccessResult::allowed();
        }
        break;

    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * Test for given 'own' permission.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param string $operation
   *   Operation being performed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account accessing entity.
   * @param bool $check_own
   *   $check_own=TRUE to check own permission, otherwise, `check any`.
   *
   * @return string|null
   *   The permission string indicating it's allowed.
   */
  protected function checkAnyOwnAccess(EntityInterface $entity, string $operation, AccountInterface $account, bool $check_own): ?string {
    if ($check_own === TRUE) {
      $uid = $entity->getOwnerId();

      $is_own = $account->isAuthenticated() && (int) $account->id() === $uid;
      if (!$is_own) {
        return NULL;
      }
    }

    $ops = [
      'create' => 'add new openstack tenant project',
      'view' => 'view %any_own openstack tenant project',
      'update' => 'edit %any_own openstack tenant project',
      'delete' => 'delete %any_own openstack tenant project',
    ];
    $permission = strtr($ops[$operation], ['%any_own' => ($check_own === TRUE) ? 'own' : 'any']);

    if ($account->hasPermission($permission)) {
      return $permission;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add new openstack tenant project');
  }

}
