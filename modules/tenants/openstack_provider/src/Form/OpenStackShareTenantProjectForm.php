<?php

namespace Drupal\openstack_provider\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\facade\Entity\Tenant;

/**
 * Share a project with another user.
 */
class OpenStackShareTenantProjectForm extends OpenStackProviderBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openstack_provider_share_tenant_project';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $project = $this->routeMatch->getParameter('openstack_tenant_project');

    $form['project'] = [
      '#type' => 'item',
      '#title' => $this->t('Share the project %project', [
        '%project' => $project->getName(),
      ]),
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#required' => TRUE,
      '#description' => $this->t('Specify the username to share the project with.'),
      '#ajax' => [
        'callback' => '::tenantCallback',
        'event' => 'change',
      ],
    ];

    $form['tenant'] = [
      '#type' => 'select',
      '#title' => $this->t('Tenant'),
      '#description' => $this->t('Select the tenant to share project.'),
      '#required' => TRUE,
      '#options' => !empty($form_state->getValue('username')) ? $this->getUserTenants($form_state->getValue('username')) : [],
      '#prefix' => '<div id="tenant-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Share project'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Don't validate during ajax form rebuild.
    if ($this->getRequest()->isXmlHttpRequest()) {
      return;
    }
    // Validate that the target project/uid combo doesn't exist already.
    $project = $this->routeMatch->getParameter('openstack_tenant_project');
    if ($this->projectExists($form_state->getValue('username'), $project->getProjectId()) === TRUE) {
      $form_state->setErrorByName('username', $this->t('Project %project already exists for @user', [
        '%project' => $project->getName(),
        '@user' => $form_state->getValue('username'),
      ]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    // Clone the project and save it to the target tenant.
    /** @var \Drupal\openstack_provider\Entity\OpenStackTenantProject $project */
    $project = $this->routeMatch->getParameter('openstack_tenant_project');
    $current_tenant = $this->routeMatch->getParameter('tenant');
    $target_tenant = Tenant::load($form_state->getValue('tenant'));
    $uid = $this->getUserId($form_state->getValue('username'));

    if (empty($target_tenant)) {
      return;
    }

    $form_state->setRedirect('entity.tenant.canonical', [
      'tenant' => $current_tenant->id(),
    ]);

    try {
      $cloud_config = $target_tenant->get('field_openstack_region')->entity;
      if (empty($cloud_config)) {
        $this->messenger->addError($this->t('No OpenStack region found.'));
        return;
      }

      $new_project = $project->createDuplicate();
      $new_project->setUserId($uid);
      $new_project->setTenant($target_tenant->id());
      $new_project->setSharedProject(TRUE);
      $new_project->save();

      // Add it to target Tenant.
      $new_cloud_config = $this->duplicateCloudConfig(
        $cloud_config,
        [
          'name' => "Shared: {$new_project->getName()}",
          'field_project_id' => $new_project->getProjectId(),
          'field_username' => $new_project->getUsername(),
          'field_password' => $new_project->getPassword(),
        ]);

      if (empty($new_cloud_config)) {
        $this->messenger->addError($this->t('Unable to create OpenStack region.'));
        return;
      }
      $params = $this->normalizeCloudConfig($new_cloud_config);
      if (empty($params)) {
        $this->messenger->addError($this->t('Unable to normalize parameters.'));
        return;
      }
      $this->openStackProviderService->createRemoteCloudConfig($target_tenant, $params);
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Unable to share project. @exception', [
        '@exception' => $e->getMessage(),
      ]));
    }
    $this->messenger->addStatus($this->t('Shared project @project', [
      '@project' => $project->getName(),
    ]));
  }

  /**
   * Ajax callback function to update tenant.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns the data back as AjaxResponse object.
   */
  public function tenantCallback(array $form, FormStateInterface $form_state): AttachmentsInterface {
    $username = $form_state->getValue('username');
    if (empty($username)) {
      // Empty Ajax command?
      return new AjaxResponse();
    }
    $tenants = $form['tenant'];
    $tenants['#options'] = $this->getUserTenants($username);
    $response = new AjaxResponse();
    $response->addCommand(
      new ReplaceCommand('#tenant-wrapper', $tenants)
    );
    return $response;
  }

  /**
   * Check if an OpenStack tenant project exists.
   *
   * @param string $username
   *   Username to check.
   * @param string $project_id
   *   Project to check.
   *
   * @return bool
   *   TRUE if exists, FALSE otherwise.
   */
  private function projectExists(string $username, string $project_id): bool {
    $exists = FALSE;
    $uid = $this->getUserId($username);
    if (empty($uid)) {
      return FALSE;
    }
    try {
      $results = $this->entityTypeManager->getStorage('openstack_tenant_project')
        ->loadByProperties([
          'uid' => $uid,
          'project_id' => $project_id,
        ]);
      if (!empty($results)) {
        $exists = TRUE;
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('An error occurred @error.', [
        '@error' => $e->getMessage(),
      ]));
      $exists = FALSE;
    }
    return $exists;
  }

  /**
   * Get a user's tenants.
   *
   * @param string $username
   *   The user_id to query.
   *
   * @return array
   *   Array of tenants.
   */
  private function getUserTenants(string $username): array {
    $tenant = $this->routeMatch->getParameter('tenant');
    $uid = $this->getUserId($username);
    if (empty($tenant) || empty($username) || empty($uid)) {
      return [];
    }
    $options = [];
    try {
      $ids = $this->entityTypeManager->getStorage('tenant')
        ->getQuery()
        ->accessCheck()
        ->condition('uid', $uid)
        ->condition('type', 'openstack')
        ->condition('id', [$tenant->id()], 'NOT IN')
        ->execute();
      $entities = $this->entityTypeManager->getStorage('tenant')
        ->loadMultiple($ids);

      foreach ($entities ?: [] as $entity) {
        $options[$entity->id()] = $entity->label();
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('An error occurred @error.', [
        '@error' => $e->getMessage(),
      ]));
      $options = [];
    }
    return $options;
  }

  /**
   * Helper to get userid from username.
   *
   * @param string $username
   *   Username to query.
   *
   * @return int|null
   *   User id or null.
   */
  private function getUserId(string $username): ?int {
    $id = NULL;
    try {
      $results = $this->entityTypeManager
        ->getStorage('user')
        ->loadByProperties([
          'name' => $username,
        ]);

      if (!empty($results)) {
        $user = array_shift($results);
        $id = $user->id();
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('An error occurred @error.', [
        '@error' => $e->getMessage(),
      ]));
      $id = NULL;
    }
    return $id;
  }

}
