<?php

namespace Drupal\openstack_provider\Form\Config;

use Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface;
use Drupal\cloud\Service\CloudServiceInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\TimeZoneFormHelper;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManager;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\openstack_provider\Service\OpenStackProviderServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * OpenStack launch form configurations.
 */
class OpenStackProviderAdminSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\cloud\Service\CloudServiceInterface definition.
   *
   * @var \Drupal\cloud\Service\CloudServiceInterface
   */
  protected $cloudService;

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * OpenStack provider service.
   *
   * @var \Drupal\openstack_provider\Service\OpenStackProviderServiceInterface
   */
  protected $openStackProviderService;

  /**
   * OpenStackProviderAdminSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\cloud\Service\CloudServiceInterface $cloud_service
   *   The cloud service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger Service.
   * @param \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface $cloud_config_plugin_manager
   *   The cloud service provider plugin manager (CloudConfigPluginManager).
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager object.
   * @param \Drupal\openstack_provider\Service\OpenStackProviderServiceInterface $openstack_provider_service
   *   The openstack provider service object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CloudServiceInterface $cloud_service,
    MessengerInterface $messenger,
    CloudConfigPluginManagerInterface $cloud_config_plugin_manager,
    AccountInterface $current_user,
    EntityTypeManager $entity_type_manager,
    OpenStackProviderServiceInterface $openstack_provider_service,
  ) {
    parent::__construct($config_factory);
    $this->configFactory = $config_factory;
    $this->cloudService = $cloud_service;
    $this->messenger = $messenger;
    $this->cloudConfigPluginManager = $cloud_config_plugin_manager;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->openStackProviderService = $openstack_provider_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cloud'),
      $container->get('messenger'),
      $container->get('plugin.manager.cloud_config_plugin'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('openstack_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'openstack_provider.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openstack_provider_admin_settings';
  }

  /**
   * Ajax callback function to update vpc, and subnets.
   *
   * This method is called when the target provider option changes.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function vpcCallback(array &$form, FormStateInterface $form_state) {
    return $form['aws']['network'];
  }

  /**
   * Ajax callback function to retrieve subnets.
   *
   * This method is called when the VPC option is changed.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function subnetCallback(array &$form, FormStateInterface $form_state) {
    return $form['aws']['network']['subnets'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('openstack_provider.settings');

    // Use the triggering_element to clear the #default_value of any #select
    // element that uses AJAX to update the select options.  Keep this
    // block of code in the buildForm() method.  If moved to the #ajax
    // callbacks, the form will sometimes be cached and cause an error.
    $triggering_element = $form_state->getTriggeringElement();
    $vpc_id = $config->get('vpc') ?? '';
    $target_provider = $form_state->getValue('target_provider') ?? $config->get('target_provider') ?? '';
    $public_subnet_1 = $form_state->getValue('public_subnet_1') ?? $config->get('public_subnet_1') ?? '';

    // Clear any selected values during an ajax callback.
    if (!empty($triggering_element)) {
      switch ($triggering_element['#name']) {
        case 'target_provider':
          $target_provider = $triggering_element['#value'];
          $vpc_id = '';
          $public_subnet_1 = '';
          break;

        case 'vpc':
          $vpc_id = $triggering_element['#value'];
          $public_subnet_1 = '';
          break;
      }
    }

    $form = parent::buildForm($form, $form_state);
    $form['openstack'] = [
      '#type' => 'details',
      '#title' => $this->t('OpenStack'),
      '#open' => TRUE,
    ];

    $providers = $this->getProviders('openstack');
    $form['openstack']['selectable_regions'] = [
      '#type' => 'select',
      '#title' => $this->t('Selectable OpenStack regions'),
      '#description' => $this->t('Regions available to users during tenant creation.'),
      '#options' => $providers,
      '#default_value' => $config->get('selectable_regions'),
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#size' => count($providers),
    ];

    $form['openstack']['autocreate_projects'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatically create projects'),
      '#description' => $this->t('If checked, automatically create projects when creating a tenant.'),
      '#default_value' => $config->get('autocreate_projects'),
    ];

    $form['openstack']['additional_roles'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional roles for OpenStack users'),
      '#description' => $this->t('During OpenStack resource creation, assign additional roles to the user. For specifying multiple roles, use a comma to separate them.  For example, "role1,role2,role3".'),
      '#default_value' => $config->get('additional_roles'),
    ];

    $form['aws'] = [
      '#type' => 'details',
      '#title' => $this->t('AWS Settings'),
      '#open' => TRUE,
    ];
    $form['aws']['target_provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Target provider'),
      '#required' => TRUE,
      '#options' => $this->getProviders('aws_cloud', 'getCloudContext', TRUE),
      '#description' => $this->t('Select region to launch Cloud Orchestrator instances.'),
      '#ajax' => [
        'callback' => '::vpcCallback',
        'event' => 'change',
        'wrapper' => 'network-container',
      ],
      '#default_value' => $target_provider,
    ];

    $form['aws']['network'] = [
      '#type' => 'details',
      '#title' => $this->t('Network'),
      '#open' => TRUE,
      '#attributes' => [
        'id' => 'network-container',
      ],
    ];

    $form['aws']['network']['vpc'] = [
      '#type' => 'select',
      '#title' => $this->t('VPC'),
      '#description' => $this->t('Select a VPC to use with this stack.'),
      '#required' => TRUE,
      '#options' => $this->getVpcs($target_provider),
      '#default_value' => $vpc_id,
      '#ajax' => [
        'callback' => '::subnetCallback',
        'event' => 'change',
        'wrapper' => 'subnet-container',
      ],
    ];

    $form['aws']['network']['subnets'] = [
      '#type' => 'details',
      '#title' => $this->t('Subnets'),
      '#attributes' => [
        'id' => 'subnet-container',
      ],
      '#open' => TRUE,
    ];

    $form['aws']['network']['subnets']['public_subnet_1'] = [
      '#type' => 'select',
      '#title' => $this->t('Public subnet'),
      '#description' => $this->t('Select a public subnet to use with the EC2 instance.'),
      '#options' => $this->getSubnets($target_provider, $vpc_id),
      '#required' => TRUE,
      '#default_value' => $public_subnet_1,
    ];

    $form['aws']['rds'] = [
      '#type' => 'details',
      '#title' => $this->t('RDS'),
      '#open' => TRUE,
    ];

    $form['aws']['rds']['rds_instance_secret_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('RDS secret name'),
      '#description' => $this->t('The AWS Secret name containing RDS configuration information.'),
      '#default_value' => $config->get('rds_instance_secret_name'),
      '#required' => TRUE,
    ];

    $form['aws']['memcache'] = [
      '#type' => 'details',
      '#title' => $this->t('Memcache'),
      '#open' => TRUE,
    ];

    $form['aws']['memcache']['memcache_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Memcache address'),
      '#description' => $this->t('Memcache endpoint'),
      '#default_value' => $config->get('memcache_address'),
      '#required' => TRUE,
    ];

    $form['aws']['memcache']['memcache_port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Memcache port'),
      '#description' => $this->t('Memcache port'),
      '#default_value' => $config->get('memcache_port'),
      '#required' => TRUE,
    ];

    $form['aws']['dns'] = [
      '#type' => 'details',
      '#title' => $this->t('Route 53'),
      '#open' => TRUE,
    ];

    $form['aws']['dns']['hosted_zone_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hosted zone name'),
      '#description' => $this->t('The hosted zone name (ex: example.com)'),
      '#default_value' => $config->get('hosted_zone_name'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs'] = [
      '#type' => 'details',
      '#title' => $this->t('ECS'),
      '#open' => TRUE,
    ];

    $form['aws']['ecs']['ecs_cluster_arn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ECS cluster ARN'),
      '#description' => $this->t('ARN of the ECS cluster ARN that the tenant is installed into.'),
      '#default_value' => $config->get('ecs_cluster_arn'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['image_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Docker URL'),
      '#description' => $this->t('Docker URL of Cloud Orchestrator installation image'),
      '#default_value' => $config->get('image_uri'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['installation_lambda_arn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Installation lambda ARN'),
      '#description' => $this->t('Installation lambda ARN'),
      '#default_value' => $config->get('installation_lambda_arn'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['ecs_task_definition_arn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ECS task definition ARN'),
      '#description' => $this->t('ECS task definition ARN'),
      '#default_value' => $config->get('ecs_task_definition_arn'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['ecs_security_group'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ECS security group ID'),
      '#description' => $this->t('ECS security group ID'),
      '#default_value' => $config->get('ecs_security_group'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['facade_task_definition_container_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facade task definition container name'),
      '#description' => $this->t('The container name of the Facade tenant creation definition. The default "facade" should not need to be changed.'),
      '#default_value' => $config->get('facade_task_definition_container_name'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['efs_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('EFS ID'),
      '#description' => $this->t('EFS ID'),
      '#default_value' => $config->get('efs_id'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['efs_access_point_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('EFS access point ID'),
      '#description' => $this->t('EFS access point ID'),
      '#default_value' => $config->get('efs_access_point_id'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['load_balancer_dns_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Load balancer DNS name'),
      '#description' => $this->t('The FQDN for the load balancer.'),
      '#default_value' => $config->get('load_balancer_dns_name'),
      '#required' => TRUE,
    ];

    $form['aws']['ecs']['lambda_timeout'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Lambda timeout'),
      '#description' => $this->t('Lambda timeout before the stack errors out.'),
      '#default_value' => $config->get('lambda_timeout'),
      '#required' => TRUE,
    ];

    $form['cfn'] = [
      '#type' => 'details',
      '#title' => $this->t('CloudFormation'),
      '#open' => TRUE,
    ];

    $form['cfn']['cloud_orchestrator_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cloud Orchestrator version'),
      '#description' => $this->t('Version of the Cloud Orchestrator to install.'),
      '#default_value' => $config->get('cloud_orchestrator_version'),
      '#required' => TRUE,
    ];

    $form['cfn']['github_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Github token'),
      '#description' => $this->t('Github token to avoid API rate limits.'),
      '#default_value' => $config->get('github_token'),
      '#required' => TRUE,
    ];

    $form['drupal'] = [
      '#type' => 'details',
      '#title' => $this->t('Drupal'),
      '#open' => TRUE,
    ];

    $form['drupal']['drupal_site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name'),
      '#default_value' => $config->get('drupal_site_name'),
      '#description' => $this->t('Site name for Cloud Orchestrator instance.'),
      '#required' => TRUE,
    ];

    $form['drupal']['drupal_site_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Site email'),
      '#description' => $this->t('The From address in automated emails sent during registration and new password requests, and other notifications.'),
      '#default_value' => $config->get('drupal_site_email'),
      '#required' => TRUE,
    ];

    $form['drupal']['drupal_enduser_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('End user username'),
      '#default_value' => $config->get('drupal_enduser_username'),
      '#description' => $this->t('A second administrative user used for Cloud Orchestrator management and troubleshooting.'),
      '#required' => TRUE,
    ];

    $form['drupal']['drupal_enduser_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('End user email'),
      '#default_value' => $config->get('drupal_enduser_email'),
      '#description' => $this->t("A second administrative user's email address."),
      '#required' => TRUE,
    ];

    $form['drupal']['welcome_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Welcome email subject'),
      '#default_value' => $config->get('welcome_subject'),
      '#description' => $this->t('Welcome email subject used for sending welcome email.'),
      '#required' => TRUE,
    ];

    $form['drupal']['welcome_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Welcome email message'),
      '#default_value' => $config->get('welcome_message'),
      '#description' => $this->t('Welcome email body used for sending welcome email.  The variables {$LOGIN_LINK}, {$DNS_NAME}, {$CO_ADMIN_ID} are available for substitutions. "\r\n" are needed for newline characters.'),
      '#required' => TRUE,
    ];

    $form['drupal']['use_spa_ui'] = [
      '#type' => 'select',
      '#options' => [
        'true' => $this->t('Use React SPA UI'),
        'false' => $this->t('Use Drupal UI'),
      ],
      '#title' => $this->t('Use SPA UI'),
      '#default_value' => $config->get('use_spa_ui'),
      '#description' => $this->t('Use React SPA UI or keep traditional Drupal UI.'),
      '#required' => TRUE,
    ];

    $zones = TimeZoneFormHelper::getOptionsListByRegion();

    $form['drupal']['drupal_timezone'] = [
      '#type' => 'select',
      '#options' => $zones,
      '#title' => $this->t('Timezone'),
      '#default_value' => $config->get('drupal_timezone'),
      '#description' => $this->t('Default timezone.'),
      '#required' => TRUE,
    ];

    $form['drupal']['country'] = [
      '#type' => 'select',
      '#options' => CountryManager::getStandardList(),
      '#title' => $this->t('Country'),
      '#description' => $this->t('Default country for location map.'),
      '#default_value' => $config->get('country'),
      '#required' => TRUE,
    ];

    $form['drupal']['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#required' => TRUE,
      '#description' => $this->t('Default city for location map.'),
      '#default_value' => $config->get('city'),
    ];

    $form['drupal']['zoom_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Zoom level'),
      '#required' => TRUE,
      '#default_value' => $config->get('zoom_level'),
      '#description' => $this->t('Default zoom level for map.'),
      '#options' => $this->openStackProviderService->getLocationMapZoomLevels(),
    ];

    $form['drupal']['enable_smtp'] = [
      '#type' => 'select',
      '#options' => [
        'true' => $this->t('Use Amazon SES'),
        'false' => $this->t('Use local email agent'),
      ],
      '#title' => $this->t('Enable SMTP'),
      '#default_value' => $config->get('enable_smtp'),
      '#description' => $this->t('Select which email agent to send system emails.'),
      '#required' => TRUE,
    ];

    $form['drupal']['smtp_secret_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SMTP Secret'),
      '#description' => $this->t('AWS Secret containing SMTP configuration information.'),
      '#default_value' => $config->get('smtp_secret_name'),
    ];

    $form['debug'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug'),
      '#open' => TRUE,
    ];

    $form['debug']['disable_ssl_verify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable SSL verification in Guzzle client during Facade to Cloud Orchestrator REST API calls'),
      '#description' => $this->t('IMPORTANT: Do no enable in production environment. This should only be used in development or testing where Cloud Orchestrator is behind a self-signed certificate.'),
      '#default_value' => $config->get('disable_ssl_verify'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $enable_smtp = (bool) $form_state->getValue('enable_smtp');
    $smtp_secret_name = $form_state->getValue('smtp_secret_name');
    if ($enable_smtp === TRUE && empty($smtp_secret_name)) {
      $form_state->setErrorByName('smtp_secret_name', $this->t('SMTP Secret needs to be set if using Amazon SES.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory()->getEditable('openstack_provider.settings');

    foreach ($form_state->cleanValues()->getValues() ?: [] as $key => $value) {
      if ($key === 'selectable_regions') {
        // This is a multiselect field.
        $config->set($key, $value);
      }
      else {
        $config->set($key, Html::escape($value));
      }
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Get a list of cloud providers based on entity_bundle.
   *
   * @param string $entity_bundle
   *   The bundle to retrieve.
   * @param string $method_name
   *   The method name to act as the key.
   * @param bool $add_none
   *   Add an empty option.
   *
   * @return array
   *   An array list of providers.
   */
  private function getProviders(string $entity_bundle, string $method_name = 'id', bool $add_none = FALSE): array {
    $providers = [];
    if ($add_none === TRUE) {
      $providers['_none'] = $this->t('- Select -');
    }
    if (empty($entity_bundle)) {
      return [];
    }
    $entities = $this->cloudConfigPluginManager->loadConfigEntities($entity_bundle);
    foreach ($entities ?: [] as $entity) {
      if ($this->currentUser->hasPermission('view all cloud service providers') || $this->currentUser->hasPermission('view ' . $entity->getCloudContext())) {
        $providers[$entity->$method_name()] = $entity->label();
      }
    }
    return $providers;
  }

  /**
   * Get vpcs depending on target provider.
   *
   * @param string $target_provider
   *   The target provider.
   *
   * @return array
   *   An array of vpcs formatted for a select box.
   */
  private function getVpcs(string $target_provider): array {
    $vpcs = [
      '_none' => $this->t('- Select -'),
    ];
    try {
      $entities = $this->entityTypeManager
        ->getStorage('aws_cloud_vpc')
        ->loadByProperties(['cloud_context' => $target_provider]);

      foreach ($entities ?: [] as $entity) {
        if ($this->currentUser->hasPermission('view all cloud service providers') || $this->currentUser->hasPermission('view ' . $entity->getCloudContext())) {
          $vpcs[$entity->getVpcId()] = $this->t('%label', [
            '%label' => $entity->label(),
          ]);
        }
      }

    }
    catch (\Exception $e) {
      $this->cloudService->handleException($e);
      $vpcs = [];
    }
    return $vpcs;
  }

  /**
   * Get subnets depending on target provider.
   *
   * @param string $target_provider
   *   The target provider.
   * @param string $vpc_id
   *   The vpc_id to query from.
   *
   * @return array
   *   An array of subnets formatted for a select box.
   */
  private function getSubnets(string $target_provider, string $vpc_id): array {
    $subnets = [
      '_none' => $this->t('- Select -'),
    ];
    try {
      $entities = $this->entityTypeManager
        ->getStorage('aws_cloud_subnet')
        ->loadByProperties([
          'cloud_context' => $target_provider,
          'vpc_id' => $vpc_id,
        ]);

      foreach ($entities ?: [] as $entity) {
        if ($this->currentUser->hasPermission('view all cloud service providers') || $this->currentUser->hasPermission('view ' . $entity->getCloudContext())) {
          $subnets[$entity->getSubnetId()] = $this->t('%label - (%cidr)', [
            '%label' => $entity->label(),
            '%cidr' => $entity->getCidrBlock(),
          ]);
        }
      }
    }
    catch (\Exception $e) {
      $this->cloudService->handleException($e);
      $subnets = [];
    }
    return $subnets;
  }

}
