<?php

namespace Drupal\openstack_provider\Form;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Adds a new project to an existing Tenant.
 */
class OpenStackAddNewTenantProjectForm extends OpenStackProviderBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'openstack_provider_add_new_tenant_project';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['regions'] = [
      '#type' => 'select',
      '#title' => $this->t('OpenStack regions'),
      '#description' => $this->t('Select region for new project'),
      '#options' => $this->getOpenStackRegions(),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project name'),
      '#required' => TRUE,
      '#description' => $this->t('Specify new project name.'),
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add new project'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $tenant = $this->routeMatch->getParameter('tenant');

    try {
      $cloud_config = $this->openStackProviderService->getRemoteCloudConfigByName($tenant, $form_state->getValue('name'));
      if (!empty($cloud_config)) {
        $form_state->setErrorByName('name', $this->t('Project name already exists'));
      }
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Unable to continue creating project. @exception', [
        '@exception' => $e->getMessage(),
      ]));
      $form_state->setErrorByName('name', $this->t('Error occurred contacting Cloud Orchestrator. Please try again later.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\facade\Entity\Tenant $tenant */
    $tenant = $this->routeMatch->getParameter('tenant');
    $cloud_context = $form_state->getValue('regions');
    $name = $form_state->getValue('name');
    $this->cloudConfigPluginManager->setCloudContext($cloud_context);
    $cloud_config = $this->cloudConfigPluginManager->loadConfigEntity();

    $form_state->setRedirect('entity.tenant.canonical', [
      'tenant' => $tenant->id(),
    ]);

    if (empty($cloud_config) || empty($tenant)) {
      $this->messenger->addError($this->t('Cannot continue adding project. Selected region and/or tenant not found.'));
      return;
    }
    // Create the project.
    try {
      $project = $this->openStackProviderService->createOpenStackResource($cloud_config, $tenant, $name, $name);
      // Prep the cloud config object for REST api by taking the selected
      // cloud config region, and setting up the newly created project id,
      // username and password.
      $new_cloud_config = $this->duplicateCloudConfig(
        $cloud_config,
        [
          'name' => $name,
          'field_project_id' => $project->getProjectId(),
          'field_username' => $project->getUsername(),
          'field_password' => $project->getPassword(),
        ]);
      if (empty($new_cloud_config)) {
        $this->messenger->addError($this->t('Unable to create OpenStack region.'));
        return;
      }
      $params = $this->normalizeCloudConfig($new_cloud_config);
      if (empty($params)) {
        $this->messenger->addError($this->t('Unable to normalize parameters.'));
        return;
      }
      $this->openStackProviderService->createRemoteCloudConfig($tenant, $params);
      $this->messenger->addStatus($this->t('Project "@name" created.', [
        '@name' => $name,
      ]));
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Unable to continue creating project. @exception', [
        '@exception' => $e->getMessage(),
      ]));
    }
    catch (ExceptionInterface $e) {
      // Catch Symfony normalize() exceptions.
      $this->messenger->addError($this->t('Unable to continue creating project. @exception', [
        '@exception' => $e->getMessage(),
      ]));
    }

  }

  /**
   * Get selectable OpenStack regions.
   *
   * @return array
   *   Array of regions.
   */
  private function getOpenStackRegions(): array {
    $options = [];
    $selectable_regions = $this->configFactory->get('openstack_provider.settings')
      ->get('selectable_regions');
    try {
      $results = $this->entityTypeManager
        ->getStorage('cloud_config')
        ->loadMultiple($selectable_regions);

      foreach ($results ?: [] as $result) {
        $options[$result->getCloudContext()] = $result->label();
      }
    }
    catch (\Exception $e) {
      $options = [];
    }
    return $options;
  }

}
