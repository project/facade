<?php

namespace Drupal\openstack_provider\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting OpenStack tenant projects.
 *
 * @ingroup openstack_provider
 */
class OpenStackTenantProjectDeleteForm extends ContentEntityDeleteForm {


}
