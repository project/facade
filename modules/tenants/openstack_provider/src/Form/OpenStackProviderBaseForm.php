<?php

namespace Drupal\openstack_provider\Form;

use Drupal\cloud\Entity\CloudConfig;
use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Base openstack_provider form.
 */
abstract class OpenStackProviderBaseForm extends FormBase {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * Subclasses should use the self::config() method, which may be overridden to
   * address specific needs when loading config, rather than this property
   * directly. See \Drupal\Core\Form\ConfigFormBase::config() for an example of
   * this.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The cloud service provider plugin manager (CloudConfigPluginManager).
   *
   * @var \Drupal\cloud\Plugin\cloud\config\CloudConfigPluginManagerInterface
   */
  protected $cloudConfigPluginManager;

  /**
   * The OpenStackProvider service.
   *
   * @var \Drupal\openstack_provider\Service\OpenStackProviderService
   */
  protected $openStackProviderService;

  /**
   * The serializer class.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected $serializer;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->configFactory = $container->get('config.factory');
    $instance->routeMatch = $container->get('current_route_match');
    $instance->cloudConfigPluginManager = $container->get('plugin.manager.cloud_config_plugin');
    $instance->openStackProviderService = $container->get('openstack_provider');
    $instance->serializer = $container->get('serializer');
    $instance->messenger = $container->get('messenger');
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * Helper to normalize the CloudConfig.
   *
   * @param \Drupal\cloud\Entity\CloudConfig $cloud_config
   *   The CloudConfig to duplicate.
   *
   * @return array
   *   The param array.
   */
  protected function normalizeCloudConfig(CloudConfig $cloud_config): array {
    $params = [];
    try {
      $params = $this->serializer->normalize($cloud_config, 'json');
      unset(
        $params['id'],
        $params['uuid'],
        $params['vid'],
        $params['changed'],
        $params['uid'],
        $params['revision_created'],
        $params['revision_user'],
        $params['revision_log_message'],
        $params['created']
      );
    }
    catch (ExceptionInterface $e) {
      // Catch Symfony normalize() exceptions.
      $this->messenger->addError($this->t('Unable to continue creating project. @exception', [
        '@exception' => $e->getMessage(),
      ]));
    }
    return $params;
  }

  /**
   * Helper to duplicate a CloudConfig with new parameters.
   *
   * @param \Drupal\cloud\Entity\CloudConfig $cloud_config
   *   The CloudConfig to duplicate.
   * @param array $params
   *   An array of field parameters.
   *
   * @return \Drupal\cloud\Entity\CloudConfig|null
   *   The new CloudConfig entity.
   */
  protected function duplicateCloudConfig(CloudConfig $cloud_config, array $params): ?CloudConfig {
    if (empty($params) || empty($cloud_config)) {
      return NULL;
    }
    $new_cloud_config = $cloud_config->createDuplicate();
    foreach ($params ?: [] as $key => $value) {
      if ($new_cloud_config->hasField($key)) {
        $new_cloud_config->set($key, $value);
      }
    }
    return $new_cloud_config;
  }

}
