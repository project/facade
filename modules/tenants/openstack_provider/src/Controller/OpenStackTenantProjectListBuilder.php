<?php

namespace Drupal\openstack_provider\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of OpenStack tenant projects.
 *
 * @ingroup openstack_provider
 */
class OpenStackTenantProjectListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Project ID');
    $header['name'] = $this->t('Name');
    $header['user'] = $this->t('Owner');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\openstack_provider\Entity\OpenStackTenantProject $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.openstack_tenant_project.edit_form',
      [
        'openstack_tenant_project' => $entity->id(),
        'tenant' => $entity->getTenant()->id(),
      ]
    );
    $row['user'] = $entity->getOwner()->getAccountName();
    return $row + parent::buildRow($entity);
  }

}
