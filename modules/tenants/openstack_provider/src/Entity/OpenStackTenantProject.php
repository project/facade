<?php

namespace Drupal\openstack_provider\Entity;

use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\facade\Entity\TenantInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Tenant Project entity.
 *
 * @ingroup openstack_provider
 *
 * @ContentEntityType(
 *   id = "openstack_tenant_project",
 *   label = @Translation("OpenStack tenant project"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\openstack_provider\Controller\OpenStackTenantProjectListBuilder",
 *     "views_data" = "Drupal\openstack_provider\Entity\OpenStackTenantProjectViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\openstack_provider\Form\OpenStackTenantProjectForm",
 *       "add" = "Drupal\openstack_provider\Form\OpenStackTenantProjectForm",
 *       "add-to-tenant" = "Drupal\openstack_provider\Form\OpenStackAddNewTenantProjectForm",
 *       "share" = "\Drupal\openstack_provider\Form\OpenStackShareTenantProjectForm",
 *       "edit" = "Drupal\openstack_provider\Form\OpenStackTenantProjectForm",
 *       "delete" = "Drupal\openstack_provider\Form\OpenStackTenantProjectDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\openstack_provider\Access\OpenStackTenantProjectAccessControlHandler",
 *   },
 *   base_table = "openstack_tenant_project",
 *   translatable = FALSE,
 *   fieldable = FALSE,
 *   admin_permission = "administer openstack tenant project entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/facade/tenant/{tenant}/project/{openstack_tenant_project}",
 *     "add-form" = "/facade/tenant/project/add",
 *     "add-to-tenant-form" = "/facade/tenant/{tenant}/project/add",
 *     "share-form" = "/facade/tenant/{tenant}/project/{openstack_tenant_project}/share",
 *     "edit-form" = "/facade/tenant/{tenant}/project/{openstack_tenant_project}/edit",
 *     "delete-form" = "/facade/tenant/{tenant}/project/{openstack_tenant_project}/delete",
 *     "collection" = "/admin/structure/project",
 *   }
 * )
 */
class OpenStackTenantProject extends ContentEntityBase implements OpenStackTenantProjectInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): ?string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name): OpenStackTenantProjectInterface {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): ?int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): OpenStackTenantProjectInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): ?UserInterface {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): ?int {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): int {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): OpenStackTenantProjectInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTenant(): ?TenantInterface {
    return $this->get('tenant')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setTenant(int $tenant_id): OpenStackTenantProjectInterface {
    $this->set('tenant', $tenant_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOpenStackRegion(): ?CloudConfigInterface {
    return $this->get('openstack_region')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOpenStackRegion(int $openstack_region_id): OpenStackTenantProjectInterface {
    $this->set('openstack_region', $openstack_region_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsername(): ?string {
    return $this->get('username')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUsername(string $username): OpenStackTenantProjectInterface {
    $this->set('username', $username);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPassword(): ?string {
    return $this->get('password')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPassword(string $password): OpenStackTenantProjectInterface {
    $this->set('password', $password);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectId(): ?string {
    return $this->get('project_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectId(string $project_id): OpenStackTenantProjectInterface {
    $this->set('project_id', $project_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomainId(): ?string {
    return $this->get('domain_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDomainId(string $domain_id): OpenStackTenantProjectInterface {
    $this->set('domain_id', $domain_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserId(): ?string {
    return $this->get('user_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserId(string $user_id): OpenStackTenantProjectInterface {
    $this->set('user_id', $user_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoleId(): ?string {
    return $this->get('role_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRoleId(string $role_id): OpenStackTenantProjectInterface {
    $this->set('role_id', $role_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectName(): ?string {
    return $this->get('project_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProjectName(string $project_name): OpenStackTenantProjectInterface {
    $this->set('project_name', $project_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoleName(): ?string {
    return $this->get('role_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRoleName(string $role_name): OpenStackTenantProjectInterface {
    $this->set('role_name', $role_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isSharedProject(): bool {
    return $this->get('shared')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSharedProject(bool $shared): OpenStackTenantProjectInterface {
    $this->set('shared', $shared);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('Enter the user ID of the author.'))
      ->setRevisionable(FALSE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Project entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['tenant'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Tenant'))
      ->setDescription(t('Tenant the project belongs to.'))
      ->setSetting('target_type', 'tenant')
      ->setSetting('handler', 'default')
      ->setTargetBundle('openstack')
      ->setDisplayOptions('form', [
        'type' => 'select',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', FALSE);

    $fields['openstack_region'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('OpenStack Region'))
      ->setDescription(t('Region the project belongs to.'))
      ->setSetting('target_type', 'cloud_config')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
        ],
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', FALSE);

    $fields['username'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Generated username'))
      ->setDescription(t('Username to access OpenStack.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    $fields['password'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Generated password'))
      ->setDescription(t('Password to access OpenStack.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    $fields['project_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project ID'))
      ->setDescription(t('Project ID of OpenStack.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    $fields['domain_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Domain ID'))
      ->setDescription(t('Domain ID of OpenStack.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    $fields['role_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Role ID'))
      ->setDescription(t('Role ID of OpenStack.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('User ID'))
      ->setDescription(t('User ID of OpenStack.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    $fields['project_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Project Name'))
      ->setDescription(t('Generated project name.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    $fields['role_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Role Name'))
      ->setDescription(t('Generated role name.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setRequired(TRUE);

    $fields['shared'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Shared Project')
      ->setDescription('TRUE if shared project.  Defaults to FALSE')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDefaultValue(FALSE)
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel): array {
    if (!in_array($rel, ['collection', 'add-page', 'add-form'], TRUE)) {
      // The entity ID is needed as a route parameter.
      $uri_route_parameters[$this->getEntityTypeId()] = $this->id();
    }
    $uri_route_parameters['tenant'] = $this->getTenant()->id();
    return $uri_route_parameters;
  }

}
