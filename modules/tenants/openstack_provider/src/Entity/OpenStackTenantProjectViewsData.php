<?php

namespace Drupal\openstack_provider\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for OpenStack tenant projects.
 */
class OpenStackTenantProjectViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
