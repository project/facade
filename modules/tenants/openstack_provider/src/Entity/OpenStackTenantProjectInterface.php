<?php

namespace Drupal\openstack_provider\Entity;

use Drupal\cloud\Entity\CloudConfigInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\facade\Entity\TenantInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining OpenStack tenant projects.
 *
 * @ingroup openstack_provider
 */
interface OpenStackTenantProjectInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the project name.
   *
   * @return string|null
   *   Name of the Project.
   */
  public function getName(): ?string;

  /**
   * Sets the Project name.
   *
   * @param string $name
   *   The Project name.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called Project entity.
   */
  public function setName($name): OpenStackTenantProjectInterface;

  /**
   * Gets the project creation timestamp.
   *
   * @return int|null
   *   Creation timestamp of the Project.
   */
  public function getCreatedTime(): ?int;

  /**
   * Sets the project creation timestamp.
   *
   * @param int $timestamp
   *   The Project creation timestamp.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called Project entity.
   */
  public function setCreatedTime($timestamp): OpenStackTenantProjectInterface;

  /**
   * Get the tenant entity being referenced.
   *
   * @return \Drupal\facade\Entity\TenantInterface|null
   *   The referenced Tenant entity.
   */
  public function getTenant(): ?TenantInterface;

  /**
   * Set the tenant entity reference.
   *
   * @param int $tenant_id
   *   The tenant id.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setTenant(int $tenant_id): OpenStackTenantProjectInterface;

  /**
   * Get the OpenStack region configuration entity.
   *
   * @return \Drupal\cloud\Entity\CloudConfig|null
   *   The referenced CloudConfig entity.
   */
  public function getOpenStackRegion(): ?CloudConfigInterface;

  /**
   * Set the OpenStack region configuration entity.
   *
   * @param int $openstack_region_id
   *   The openstack region id.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setOpenStackRegion(int $openstack_region_id): OpenStackTenantProjectInterface;

  /**
   * Get username for OpenStack access.
   *
   * @return string|null
   *   The configured username.
   */
  public function getUsername(): ?string;

  /**
   * Set the username for OpenStack access.
   *
   * @param string $username
   *   The username to set.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setUsername(string $username): OpenStackTenantProjectInterface;

  /**
   * Get password for OpenStack access.
   *
   * @return string|null
   *   The configured password.
   */
  public function getPassword(): ?string;

  /**
   * Set the password for OpenStack access.
   *
   * @param string $password
   *   The password to set.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setPassword(string $password): OpenStackTenantProjectInterface;

  /**
   * Get project ID for OpenStack access.
   *
   * @return string|null
   *   The configured project_id.
   */
  public function getProjectId(): ?string;

  /**
   * Set the project ID for OpenStack access.
   *
   * @param string $project_id
   *   The project_id to set.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called Project entity.
   */
  public function setProjectId(string $project_id): OpenStackTenantProjectInterface;

  /**
   * Get domain ID for OpenStack access.
   *
   * @return string|null
   *   The configured domain.
   */
  public function getDomainId(): ?string;

  /**
   * Set the domain ID for OpenStack access.
   *
   * @param string $domain_id
   *   The domain to set.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setDomainId(string $domain_id): OpenStackTenantProjectInterface;

  /**
   * Get user ID for OpenStack access.
   *
   * @return string|null
   *   The configured user_id.
   */
  public function getUserId(): ?string;

  /**
   * Set the user ID for OpenStack access.
   *
   * @param string $user_id
   *   The user_id to set.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setUserId(string $user_id): OpenStackTenantProjectInterface;

  /**
   * Get role ID for OpenStack access.
   *
   * @return string|null
   *   The configured role.
   */
  public function getRoleId(): ?string;

  /**
   * Set the user ID for OpenStack access.
   *
   * @param string $role_id
   *   The user_id to set.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setRoleId(string $role_id): OpenStackTenantProjectInterface;

  /**
   * Get project name for OpenStack.
   *
   * @return string|null
   *   The generated project name.
   */
  public function getProjectName(): ?string;

  /**
   * Set the generated project name for OpenStack.
   *
   * @param string $project_name
   *   The project name to set.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setProjectName(string $project_name): OpenStackTenantProjectInterface;

  /**
   * Get role name for OpenStack.
   *
   * @return string|null
   *   The generated role name.
   */
  public function getRoleName(): ?string;

  /**
   * Set the generated role name for OpenStack.
   *
   * @param string $role_name
   *   The project name to set.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setRoleName(string $role_name): OpenStackTenantProjectInterface;

  /**
   * Return boolean value if the project is shared from another tenant user.
   *
   * @return bool
   *   TRUE if shared, FALSE if not.
   */
  public function isSharedProject(): bool;

  /**
   * Set the boolean whether project is shared from another tenant.
   *
   * @param bool $shared
   *   Boolean valu.
   *
   * @return \Drupal\openstack_provider\Entity\OpenStackTenantProjectInterface
   *   The called project entity.
   */
  public function setSharedProject(bool $shared): OpenStackTenantProjectInterface;

}
