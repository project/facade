<?php

namespace Drupal\facade_remote_worker\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Bearer token authentication for Façade module requests.
 */
class FacadeBearerToken implements AuthenticationProviderInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager interface.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request): bool {
    $request_token = $this->getRequestToken($request);
    return !empty($request_token);
  }

  /**
   * Get the token from request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return string|null
   *   The token.
   */
  protected function getRequestToken(Request $request): ?string {
    $authorization = trim($request->headers->get('Authorization'));
    if (empty($authorization)) {
      return NULL;
    }
    if (strpos($authorization, 'Bearer ') !== 0) {
      return NULL;
    }
    $token = trim(substr($authorization, strlen('Bearer ')));
    $result = base64_decode($token);
    if ($result === FALSE) {
      return NULL;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request): ?AccountInterface {
    $request_token = $this->getRequestToken($request);
    if (empty($request_token)) {
      return NULL;
    }

    // Find user that is associated with this token.
    $user = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(
        ['field_bearer_token' => $request_token]
      );
    if (empty($user)) {
      return NULL;
    }
    return array_shift($user);
  }

}
