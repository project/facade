<?php

namespace Drupal\facade_remote_worker\Commands;

/**
 * Entity command interface.
 */
interface EntityCommandsInterface {

  /**
   * Option for specifying the file path.
   */
  public const OPT_JSON_FILE = 'file-path';

  /**
   * Option for specifying username.
   */
  public const OPT_USERNAME = 'username';

  /**
   * Create OpenStack cloud service providers.
   *
   * @param array $options
   *   Drush options.
   *
   * @return int
   *   Drush SUCCESS or FAILURE.
   */
  public function createCloudConfigs(array $options): int;

  /**
   * Add bearer token to user's field_bearer_token field.
   *
   * @param string $token
   *   The token to assign.
   * @param string $uid
   *   The uid to assign token to.
   * @param array $options
   *   Drush options.
   *
   * @return int
   *   Drush SUCCESS or FAILURE.
   */
  public function addBearerToken(string $token, string $uid = '', array $options = [self::OPT_USERNAME => NULL]): int;

}
