<?php

namespace Drupal\facade_remote_worker\Commands;

use Drupal\cloud\Entity\CloudConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drush\Commands\DrushCommands;

/**
 * Facade related drush commands.
 */
class EntityCommands extends DrushCommands implements EntityCommandsInterface {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager interface.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system interface.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    parent::__construct();
  }

  /**
   * Create cloud config entities from a json object file.
   *
   * The JSON object contains an array of Cloud Config objects to be added.
   * Each CloudConfig in the array should be formatted like such:
   *
   * $cloud_config[
   *  'name' => 'Name',
   *  'type' => 'openstack' (or 'aws_cloud' ... etc),
   *  'field_identity_api_endpoint' => '...',
   *  'field_compute_api_endpoint' => '...',
   *  ...
   * ];
   *
   * The keys should be the field_name for that particular field.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, config, etc.
   *
   * @option string $file-path
   *   The absolute file location.  For example: /tmp/foo.txt
   *
   * @return int
   *   Return the exit code.
   *
   * @command facade_remote_worker:create-cloud-configs
   * @aliases frwc, frw-ccc
   */
  public function createCloudConfigs(
    array $options = [
      self::OPT_JSON_FILE => NULL,
    ],
  ): int {

    $file = $options[self::OPT_JSON_FILE];
    if (empty($file)) {
      $this->output()->writeln(dt('No file specified.  Specify a fully qualified file.'));
      return DrushCommands::EXIT_FAILURE;
    }
    $data = file_get_contents($file);

    if (empty($data)) {
      $this->output()->writeln(dt('Unable to open file.'));
    }

    try {
      $cloud_configs = json_decode($data, TRUE, 512, JSON_THROW_ON_ERROR);
      foreach ($cloud_configs ?: [] as $cloud_config) {
        $entity = CloudConfig::create($cloud_config);
        $entity->save();
        $this->output()->writeln(dt('Saved: @entity', [
          '@entity' => $entity->label(),
        ]));
      }
    }
    catch (\Exception $e) {
      $this->output()->writeln(dt('Error during token assignment. @message', [
        '@message' => $e->getMessage(),
      ]));
      return DrushCommands::EXIT_FAILURE;
    }

    return DrushCommands::EXIT_SUCCESS;
  }

  /**
   * Assign bearer token to a particular user.
   *
   * @param string $token
   *   The token to assign.
   * @param string $uid
   *   The uid to assign token to.
   * @param array $options
   *   An associative array of options whose values come from cli, config, etc.
   *
   * @option string $username
   *   Specify username instead of uid.
   *
   * @return int
   *   Return the exit code.
   *
   * @command facade_remote_worker:add-bearer-token
   * @aliases frwa, frw-abt
   * @usage drush frw-abt token-string 2
   *   Add the "token-string" to uid=2.
   * @usage drush frw-abt token-string --username foo
   *   Add the "token-string" to username foo.
   */
  public function addBearerToken(
    string $token,
    string $uid = '',
    array $options = [
      self::OPT_USERNAME => NULL,
    ],
  ): int {

    if (empty($uid) && empty($options[self::OPT_USERNAME])) {
      $this->logger()->error(dt('uid or username has to be set.'));
      return DrushCommands::EXIT_FAILURE;
    }
    $params = [
      'uid' => $uid,
    ];
    $username = $options[self::OPT_USERNAME];
    if (!empty($username)) {
      $params = [
        'name' => $username,
      ];
    }

    try {
      $results = $this->entityTypeManager
        ->getStorage('user')
        ->loadByProperties(
          $params
        );
      if (empty($results)) {
        $this->logger()->error(dt('No user found.'));
        return DrushCommands::EXIT_FAILURE;
      }
      /** @var \Drupal\user\Entity\User $user */
      $user = array_shift($results);
      if ($user->hasField('field_bearer_token') === FALSE) {
        $this->logger()->error(dt('User does not have bearer token field.'));
        return DrushCommands::EXIT_FAILURE;
      }
      $user->set('field_bearer_token', $token);
      $user->save();
      $this->output()->writeln(dt('The token "@token" has been added for @user', [
        '@token' => $token,
        '@user' => $user->getAccountName(),
      ]));
    }
    catch (\Exception $e) {
      $this->output()->writeln(dt('Error during token assignment. @message', [
        '@message' => $e->getMessage(),
      ]));
      return DrushCommands::EXIT_FAILURE;
    }

    return DrushCommands::EXIT_SUCCESS;
  }

}
