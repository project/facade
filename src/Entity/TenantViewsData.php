<?php

namespace Drupal\facade\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Tenant entities.
 */
class TenantViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData(): array {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
