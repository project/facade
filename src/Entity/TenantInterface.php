<?php

namespace Drupal\facade\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Tenant entities.
 *
 * @ingroup facade
 */
interface TenantInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Tenant name.
   *
   * @return string
   *   Name of the Tenant.
   */
  public function getName(): string;

  /**
   * Sets the Tenant name.
   *
   * @param string $name
   *   The Tenant name.
   *
   * @return \Drupal\facade\Entity\TenantInterface
   *   The called Tenant entity.
   */
  public function setName($name): TenantInterface;

  /**
   * Gets the Tenant creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Tenant.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Tenant creation timestamp.
   *
   * @param int $timestamp
   *   The Tenant creation timestamp.
   *
   * @return \Drupal\facade\Entity\TenantInterface
   *   The called Tenant entity.
   */
  public function setCreatedTime($timestamp): TenantInterface;

}
