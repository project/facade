<?php

namespace Drupal\facade\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Tenant type entities.
 */
interface TenantTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
