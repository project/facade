<?php

namespace Drupal\facade\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Tenant type entity.
 *
 * @ConfigEntityType(
 *   id = "tenant_type",
 *   label = @Translation("Tenant type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\facade\Controller\TenantTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\facade\Form\TenantTypeForm",
 *       "edit" = "Drupal\facade\Form\TenantTypeForm",
 *       "delete" = "Drupal\facade\Form\TenantTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\facade\Routing\TenantTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   config_prefix = "tenant_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "tenant",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/tenant_type/{tenant_type}",
 *     "add-form" = "/admin/structure/tenant_type/add",
 *     "edit-form" = "/admin/structure/tenant_type/{tenant_type}/edit",
 *     "delete-form" = "/admin/structure/tenant_type/{tenant_type}/delete",
 *     "collection" = "/admin/structure/tenant_type"
 *   }
 * )
 */
class TenantType extends ConfigEntityBundleBase implements TenantTypeInterface {

  /**
   * The Tenant type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Tenant type label.
   *
   * @var string
   */
  protected $label;

}
