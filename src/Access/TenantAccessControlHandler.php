<?php

namespace Drupal\facade\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Tenant entity.
 *
 * @see \Drupal\facade\Entity\Tenant.
 */
class TenantAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResultInterface {
    /** @var \Drupal\facade\Entity\TenantInterface $entity */
    switch ($operation) {

      case 'delete':
      case 'view':
      case 'update':
        $permission = $this->checkOwn($entity, $operation, $account);
        if (!empty($permission)) {
          return AccessResult::allowed();
        }

        $permission = $this->checkAny($entity, $operation, $account);
        if (!empty($permission)) {
          return AccessResult::allowed();
        }

        break;
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResultInterface {
    if (!empty($entity_bundle)) {
      return AccessResult::allowedIfHasPermission($account, "add new $entity_bundle tenant");
    }
    return AccessResult::allowedIfHasPermission($account, 'add tenant entities');
  }

  /**
   * Test for given 'any' permission.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param string $operation
   *   Operation being performed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account accessing entity.
   *
   * @return string|null
   *   The permission string indicating it's allowed.
   */
  protected function checkAny(EntityInterface $entity, string $operation, AccountInterface $account): ?string {
    $bundle = $entity->bundle();

    $ops = [
      'create' => 'add new %bundle tenant',
      'view' => 'view any %bundle tenant',
      'update' => 'edit any %bundle tenant',
      'delete' => 'delete any %bundle tenant',
    ];
    $permission = strtr($ops[$operation], ['%bundle' => $bundle]);

    if ($account->hasPermission($permission)) {
      return $permission;
    }

    return NULL;
  }

  /**
   * Test for given 'own' permission.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param string $operation
   *   Operation being performed.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account accessing entity.
   *
   * @return string|null
   *   The permission string indicating it's allowed.
   */
  protected function checkOwn(EntityInterface $entity, string $operation, AccountInterface $account): ?string {
    $uid = $entity->getOwnerId();

    $is_own = $account->isAuthenticated() && (int) $account->id() === $uid;
    if (!$is_own) {
      return NULL;
    }

    $bundle = $entity->bundle();
    $ops = [
      'create' => 'add new %bundle tenant',
      'view' => 'view own %bundle tenant',
      'update' => 'edit own %bundle tenant',
      'delete' => 'delete own %bundle tenant',
    ];

    $permission = strtr($ops[$operation], ['%bundle' => $bundle]);

    if ($account->hasPermission($permission)) {
      return $permission;
    }

    return NULL;
  }

}
