<?php

namespace Drupal\facade\Access;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\facade\Entity\TenantType;

/**
 * Provides dynamic permissions for Tenant of different types.
 *
 * @ingroup facade
 */
class TenantPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of entity type permissions.
   *
   * @return array
   *   The Tenant by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions(): array {
    $perms = [];

    foreach (TenantType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of node permissions for a given node type.
   *
   * @param \Drupal\facade\Entity\TenantType $type
   *   The Tenant type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(TenantType $type): array {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return [
      "list $type_id tenants" => [
        'title' => $this->t('List %type_name tenants', $type_params),
      ],
      "view own $type_id tenant" => [
        'title' => $this->t('View own %type_name tenant', $type_params),
      ],
      "view any $type_id tenant" => [
        'title' => $this->t('View any %type_name tenant', $type_params),
      ],
      "add new $type_id tenant" => [
        'title' => $this->t('Add new %type_name tenant', $type_params),
      ],
      "edit own $type_id tenant" => [
        'title' => $this->t('Edit own %type_name tenant', $type_params),
      ],
      "edit any $type_id tenant" => [
        'title' => $this->t('Edit any %type_name tenant', $type_params),
      ],
      "delete own $type_id tenant" => [
        'title' => $this->t('Delete own %type_name tenant', $type_params),
      ],
      "delete any $type_id tenant" => [
        'title' => $this->t('Delete any %type_name tenant', $type_params),
      ],
    ];
  }

}
