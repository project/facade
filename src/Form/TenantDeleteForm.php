<?php

namespace Drupal\facade\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Tenant entities.
 *
 * @ingroup facade
 */
class TenantDeleteForm extends ContentEntityDeleteForm {


}
