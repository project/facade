<?php

namespace Drupal\facade\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class builds tenant type settings form.
 */
class TenantTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $tenant_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $tenant_type->label(),
      '#description' => $this->t("Label for the Tenant type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $tenant_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\facade\Entity\TenantType::load',
      ],
      '#disabled' => !$tenant_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): void {
    $tenant_type = $this->entity;
    $status = $tenant_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Tenant type.', [
          '%label' => $tenant_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Tenant type.', [
          '%label' => $tenant_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($tenant_type->toUrl('collection'));
  }

}
