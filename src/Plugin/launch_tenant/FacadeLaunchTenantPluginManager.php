<?php

namespace Drupal\facade\Plugin\launch_tenant;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\facade\Entity\TenantInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides the Facade launch tenant plugin manager plugin manager.
 */
class FacadeLaunchTenantPluginManager extends DefaultPluginManager implements FacadeLaunchTenantPluginManagerInterface {

  use StringTranslationTrait;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Default value string.
   *
   * @var string[]
   */
  protected $defaults = [
    'id' => 'facade_launch_tenant',
    'entity_type' => 'tenant',
  ];

  /**
   * Constructs a new ExamplePluginManager object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The messenger.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend, LoggerInterface $logger, Messenger $messenger) {
    // Skip calling parent constructor since that assumes
    // annotation based plugin.
    $this->moduleHandler = $module_handler;
    $this->setCacheBackend($cache_backend, 'facade_launch_tenant_plugin', ['facade_launch_tenant_plugin']);
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): DiscoveryInterface {
    $file_name_suffix = $this->getFileNameSuffix();
    if (!isset($this->discovery) && !empty($file_name_suffix)) {
      $this->discovery = new YamlDiscovery($file_name_suffix, $this->moduleHandler->getModuleDirectories());
      $this->discovery->addTranslatableProperty('label', 'label_context');
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($this->discovery);
    }
    return parent::getDiscovery();
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);
    if (empty($definition['id'])) {
      throw new PluginException(sprintf('FacadeLaunchTenantPlugin plugin property (%s) definition "is" is required.', $plugin_id));
    }

    if (empty($definition['entity_bundle'])) {
      throw new PluginException(sprintf('entity_bundle property is required for (%s)', $plugin_id));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getFileNameSuffix(): string {
    return 'facade.launch.tenant.plugin';
  }

  /**
   * {@inheritdoc}
   */
  public function loadPlugin(string $entity_bundle): ?FacadeLaunchTenantPluginInterface {
    $plugin = NULL;
    foreach ($this->getDefinitions() ?: [] as $key => $definition) {
      if ($definition['entity_type'] === 'tenant' && $definition['entity_bundle'] === $entity_bundle) {
        $plugin = $this->createInstance($key);
        break;
      }
    }
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function launch(TenantInterface $tenant): void {
    $plugin = $this->loadPlugin($tenant->bundle());
    if (empty($plugin)) {
      $this->logger->error($this->t('No plugin found.'));
      return;
    }
    $plugin->launch($tenant);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(TenantInterface $tenant): void {
    $plugin = $this->loadPlugin($tenant->bundle());
    if (empty($plugin)) {
      $this->logger->error($this->t('No plugin found.'));
      return;
    }
    $plugin->delete($tenant);
  }

}
