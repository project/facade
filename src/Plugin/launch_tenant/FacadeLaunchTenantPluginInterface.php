<?php

namespace Drupal\facade\Plugin\launch_tenant;

use Drupal\facade\Entity\TenantInterface;

/**
 * Defines an interface for Facade launch tenant plugin manager plugins.
 */
interface FacadeLaunchTenantPluginInterface {

  /**
   * Tenant launch implementation.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to launch.
   */
  public function launch(TenantInterface $tenant): void;

  /**
   * Tenant delete implementation.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to launch.
   */
  public function delete(TenantInterface $tenant): void;

}
