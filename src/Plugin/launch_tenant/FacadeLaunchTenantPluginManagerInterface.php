<?php

namespace Drupal\facade\Plugin\launch_tenant;

use Drupal\facade\Entity\TenantInterface;

/**
 * Facade launch tenant plugin manager interface.
 */
interface FacadeLaunchTenantPluginManagerInterface {

  /**
   * Load tenant plugin by entity_bundle.
   *
   * @param string $entity_bundle
   *   The entity bundle to load.
   *
   * @return \Drupal\facade\Plugin\launch_tenant\FacadeLaunchTenantPluginInterface|null
   *   The loaded plugin or null.
   */
  public function loadPlugin(string $entity_bundle): ?FacadeLaunchTenantPluginInterface;

  /**
   * Launch a tenant.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to launch.
   */
  public function launch(TenantInterface $tenant): void;

  /**
   * Delete a tenant.
   *
   * @param \Drupal\facade\Entity\TenantInterface $tenant
   *   The tenant entity to launch.
   */
  public function delete(TenantInterface $tenant): void;

}
